import 'dart:convert';
import 'package:intl/intl.dart'; // for date
import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';

ProgressDialog pr;

class HistoryModel {


  Future<HttpHandler> getTransfers(BuildContext context,DateTime date) async {
    HttpHandler _response = new HttpHandler();
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );
    var formatter = new DateFormat('yyyy-MM-dd');
    String userID = await User().getUserID();

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    try {
      final response = await http
          .get(
              'https://terablock.app/terablock/api/transfers/$userID/'+formatter.format(date))
          .timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = Transfers.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';
    }
    await pr.hide();
    return _response;
  }
}
