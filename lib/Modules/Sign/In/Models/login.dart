class Login{
  String result;
  String iduUser;
  String msj;

  Login({this.result,this.msj,this.iduUser});


  factory Login.fromJson(Map<String, dynamic> json) {
    return Login(
        result: json['result'],
        iduUser: json['user'],
        msj: json['msj']);
  }
}