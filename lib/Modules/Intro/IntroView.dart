
import 'package:TeraBlock/Others/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:TeraBlock/modules/Welcome/WelcomeView.dart';

class IntroView extends StatefulWidget {
  @override
  _IntroViewState createState() => _IntroViewState();
}

class _IntroViewState extends State<IntroView> {
  List<Slide> slides = new List();

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "",
        description:
            "Recibe, almacena y envia tus activos crypto a cualquier wallet.",
        pathImage: "assets/Icons/sliders.png",
        backgroundColor: HexColor('#0F2C45'),
      ),
    );
    slides.add(
      new Slide(
        title: "",
        description:
            "Colateraliza operaciones de trading y comerciales de terceros para generar ganancias.",
        pathImage: "assets/Icons/sliders.png",
        backgroundColor:  HexColor('#0F2C45'),
      ),
    );
    slides.add(
      new Slide(
        title: "",
        description:
            "Obten dividendos sin entregar tus fondos a nadie, solo conecta un contrato inteligente a la operación financiera de un proveedor compatible.",
        pathImage: "assets/Icons/sliders.png",
        backgroundColor:  HexColor('#0F2C45'),
      ),
    );
  }

  void onDonePress() async {
    // Do what you want
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('intro', true);

    Navigator.of(context).pop();
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WelcomeView(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      slides: this.slides,
      onDonePress: this.onDonePress,
    );
  }
}
