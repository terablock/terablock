import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/Home/HomeModel.dart';
import 'package:TeraBlock/Modules/Home/HomeView.dart';
import 'package:TeraBlock/Modules/Send/SendModel.dart';
import 'package:TeraBlock/Modules/TransferDetails/TransferDetailsView.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:flutter/material.dart';

class TransferView extends StatefulWidget {
  final Contact contact;
  TransferView({this.contact});
  @override
  _TransferViewState createState() => _TransferViewState();
}

class _TransferViewState extends State<TransferView> {
  bool internetConexion = true;
  HomeModel _homeModel = new HomeModel();
  SendModel model = new SendModel();
  UserBalance _balance = new UserBalance();
  bool amountError = false;
  bool notesError = true;
  String notes = '';
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  FocusNode myFocusAmount;
  FocusNode myFocusNote;
  TextEditingController controllerAmount = new TextEditingController();
  TextEditingController controllerNote = new TextEditingController();
  String iduTransferCode = '';
  String smsCode;
  @override
  void initState() {
    myFocusAmount = new FocusNode();
    myFocusNote = new FocusNode();
    getBalance();
    super.initState();
  }

  getBalance() async {
    HttpHandler result = await _homeModel.getBalance(context);
    if (result.result) {
      setState(() {
        _balance = result.data;
        internetConexion = true;
      });
    } else {
      //showView NO INTERNET
      setState(() {
        internetConexion = false;
      });

      showErrorHttpRequest(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        title: Text('Retiro de fondos', style: TextStyle(color: Colors.black)),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        child: getView(),
      ),
    );
  }

  Widget getView() {
    if (internetConexion == false) {
      return noConexion();
    } else if (_balance.balance == null) {
      return Column(
        children: <Widget>[showWait()],
      );
    } else {
      return Column(
        children: <Widget>[
          Card(
              margin: EdgeInsets.only(top: 10, bottom: 10, right: 20, left: 20),
              child: AvailableAmountView(
                amount: _balance.balance,
              )),
          formSend(),
        ],
      );
    }
  }

  //Screen
  Widget formSend() {
    return Expanded(
      child: ListView(
        children: [
          Column(children: [
            Padding(
              padding: EdgeInsets.all(10.0),
              child: Card(
//          color: Colors.white70,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ListTile(
                        leading: getIcon(widget.contact.type),
                        title: Text(widget.contact.name),
                        subtitle: Text(
                          widget.contact.wallet,
                          style: TextStyle(color: Colors.green),
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 15, right: 35, top: 10),
                        child: TextField(
                            focusNode: myFocusAmount,
                            keyboardType: TextInputType.numberWithOptions(
                              decimal: true,
                              signed: false,
                            ),
                            controller: controllerAmount,
                            style: Theme.of(context).textTheme.bodyText1,
                            decoration: InputDecoration(
                                errorText: !amountError
                                    ? null
                                    : 'No cuenta con fondos suficientes',
                                icon: Icon(Icons.attach_money),
                                labelText: 'Monto',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10.0)))),
                            onChanged: (val) {
                              validateAmount(val);
                            }),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 35),
                        child: TextField(
                            focusNode: myFocusNote,
                            keyboardType: TextInputType.text,
                            controller: controllerNote,
                            maxLength: 50,
                            style: Theme.of(context).textTheme.bodyText1,
                            decoration: InputDecoration(
                                icon: Icon(Icons.note),
                                labelText: 'Concepto',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10.0)))),
                            onChanged: (val) {
                              notes = val;
                            }),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(30.0),
                        child: Center(
                          child: Align(
                            alignment: Alignment.center,
                            child: ButtonTheme(
                              minWidth: MediaQuery.of(context).size.width,
                              height: 50,
                              child: RaisedButton(
                                color: Colors.blue,
                                shape: ContinuousRectangleBorder(
                                    borderRadius: BorderRadius.circular(4.0)),
                                child: Text(
                                  "Enviar",
                                  style: TextStyle(color: Colors.white),
                                ),
                                onPressed: () {
                                  verifyForm();
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ]),
        ],
      ),
    );
  }

  validateAmount(String val) {
    Pattern pattern = r'^\d+([\.]+\d{1,2})?$';
    RegExp regex = new RegExp(pattern);
    if (val.length == 1 && val == ".") {
      controllerAmount.text = '0.';
      controllerAmount.selection = TextSelection.fromPosition(
          TextPosition(offset: controllerAmount.text.length));
    } else if (val == '00') {
      controllerAmount.text = '0';
      controllerAmount.selection = TextSelection.fromPosition(
          TextPosition(offset: controllerAmount.text.length));
    } else if (double.tryParse(val) != null) {
      if (!regex.hasMatch(val) &&
          controllerAmount.text.substring(controllerAmount.text.length - 1,
                  controllerAmount.text.length) !=
              '.') {
        controllerAmount.text = val.substring(0, val.length - 1);
        controllerAmount.selection = TextSelection.fromPosition(
            TextPosition(offset: controllerAmount.text.length));
      }
    } else {
      controllerAmount.text = val.substring(0, val.length - 1);
      controllerAmount.selection = TextSelection.fromPosition(
          TextPosition(offset: controllerAmount.text.length));
    }

    if (double.parse(_balance.balance) < double.parse(controllerAmount.text) ||
        double.parse(_balance.balance) <= 1.75) {
      setState(() {
        amountError = true;
      });
    } else {
      setState(() {
        amountError = false;
      });
    }
  }

  Widget noConexion() {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              'No se puede establecer una conexión con la red',
            ),
            leading: Icon(
              Icons.error,
              color: Colors.red,
            ),
          ),
          RaisedButton(
            child: Text('Reintentar'),
            onPressed: () {
              getBalance();
            },
          )
        ],
      ),
    );
  }

  verifyForm() async {
    if (controllerAmount.text.length == 0) {
      myFocusAmount.requestFocus();
      final snackBar = SnackBar(
        content: Text('El monto es requerido'),
        backgroundColor: Colors.red,
      );
      _scaffoldkey.currentState.showSnackBar(snackBar);
    } else if (double.parse(controllerAmount.text) <= 1.75) {
      controllerAmount.text = '';
      myFocusAmount.requestFocus();
      final snackBar = SnackBar(
        content: Text('El monto debe ser mayor a 1.25'),
        backgroundColor: Colors.red,
      );
      _scaffoldkey.currentState.showSnackBar(snackBar);
    } else if (amountError || controllerAmount.text.length == 0) {
      controllerAmount.text = '';
      myFocusAmount.requestFocus();
      final snackBar = SnackBar(
        content: Text('Monto Invalido'),
        backgroundColor: Colors.red,
      );
      _scaffoldkey.currentState.showSnackBar(snackBar);
    } else if (notes.length == 0) {
      myFocusNote.requestFocus();
      final snackBar = SnackBar(
        content: Text('Concepto es requerido'),
        backgroundColor: Colors.red,
      );
      _scaffoldkey.currentState.showSnackBar(snackBar);
    } else {
      bool _result = await showDialogTransaction(
          context, widget.contact, controllerAmount.text, notes);

      if (_result) {
        bool _phoneVerify = await verifyNumberPhone(context);

        if (_phoneVerify) {
          startPhoneVerification();
        }
      }
    }
  }

  startPhoneVerification() async {
    widget.contact.amount = controllerAmount.text;
    widget.contact.note = controllerNote.text;

    HttpHandler response = await model.sendCode(context);

    if (response.result) {
      iduTransferCode = response.data;
      requireCodeVerification(context);
    } else {
      showErrorHttpRequest(context);
    }
  }

  verifyCode() async {
    HttpHandler response =
        await model.verifyCode(context, smsCode, iduTransferCode);

    if (response.result) {
      GlobalResult result = response.data;

      if (result.result == '1') {
        saveTransfer(context);
      } else {
        showErrorCode(context);
      }
    } else {
      showErrorHttpRequest(context);
    }
  }

  void saveTransfer(BuildContext context) async {
    SendModel model = new SendModel();
    HttpHandler _result =
    await model.executeTransfer(context, widget.contact, iduTransferCode);
    if (_result.result) {
      if (_result.data.result == '1') {
        Transfer transfer = new Transfer(
            idu: iduTransferCode,
            amount: widget.contact.amount,
            note: widget.contact.note,
            type: widget.contact.type.toString(),
            to: widget.contact.wallet,
            transaction: '1',
            descTransaction: 'Retiro');
        Navigator.pop(context);

        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => TransferDetails(transfer: transfer)),
        );
      } else {
        showWithoutFunds();
      }
    } else {
      showErrorHttpRequest(context);
    }
  }

  Future requireCodeVerification(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              //t
              title: Center(
                  child: Text(
                    'Ingresa código de verificación',
                    textAlign: TextAlign.center,
                  )),
              content: TextField(
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  smsCode = value;
                },
              ),
              contentPadding: EdgeInsets.all(10.0),
              actions: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    new FlatButton(
                        child: Text('Cancelar',
                            style: TextStyle(color: Colors.red)),
                        onPressed: () {
                          Navigator.pop(context, false);
                        }),
                    SizedBox(
                      width: 10,
                    ),
                    new FlatButton(
                        child: Text(
                          'Enviar',
                          style: TextStyle(color: Colors.blue),
                        ),
                        onPressed: () {
                          Navigator.pop(context, true);

                          verifyCode();
                        }),
                  ],
                )
              ]);
        });
  }

  Future showWithoutFunds() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              //t
              title: Center(
                  child: Text(
                    'Sin fondos',
                    textAlign: TextAlign.center,
                  )),
              content: Icon(
                Icons.money_off,
                size: 70,
                color: Colors.blueAccent,
              ),
              contentPadding: EdgeInsets.all(10.0),
              actions: <Widget>[
                Center(
                    child: Text(
                      'No cuentas con los fondos suficientes para realizar esta transferencia.',
                      textAlign: TextAlign.center,
                    )),
                new FlatButton(
                    child: Text('Aceptar'),
                    onPressed: () {
                      Navigator.pop(context);
                      getBalance();
                    }),
              ]);
        });
  }
}
