import 'package:TeraBlock/Others/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GlobalPin extends StatefulWidget {
  final int type;
  GlobalPin({Key key, @required this.type}) : super(key: key);
  @override
  _GlobalPinState createState() => _GlobalPinState();
}

class _GlobalPinState extends State<GlobalPin> {
  String title = 'Ingresa PIN';
  String titleBadPin = 'PIN incorrecto, intente de nuevo';
  int pinLegthCount = 0;
  String _globalPin = '159753'; //Obtenerlo de la preferencias
  String _password = '';
  String _backUpPassword = '';
  bool pinError = false;
  bool confirmPin = false;
  Color btnColor = HexColor('#0F2C45');
  @override
  void initState() {
    basicServices();
    super.initState();
  }

  void basicServices() async {
    if (widget.type == 0) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      _globalPin = prefs.getString('globalpin') ?? null;
    } else {
      title = 'Configurar PIN de 6 digitos';
    }
  }

  void setPin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('globalpin', _password);
    prefs.setBool('pin', true);
  }

  @override
  Widget build(BuildContext context) {
    void pressBtn(String value) {
      pinError = false;
      if (pinLegthCount < 4) {
        _password = _password + value;
        setState(() {
          pinLegthCount++;
        });
      }
      if (pinLegthCount == 4) {
        if (widget.type == 0) {
          //verificacion de pin
          if (_password == _globalPin) {
            Navigator.pop(context, true);
          } else {
            pinError = true;
            _password = '';
            pinLegthCount = 0;
          }
        } else {
          if (confirmPin == false) {
            //Asignar Pin
            _backUpPassword = _password;
            _password = '';
            pinLegthCount = 0;
            title = 'Confirmacion de PIN';
            confirmPin = true;
          } else {
            if (_password == _backUpPassword) {
              setPin();
              Navigator.pop(context, true);
            } else {
              _password = '';
              pinLegthCount = 0;
              confirmPin = false;
              title = 'Ingresa PIN';
              pinError = true;
              titleBadPin = 'Los PIN no coinsiden. Vuelva a intentarlo';
            }
          }
        }
      }
    }

    final Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: SafeArea(
      child: Container(
          child: Column(children: <Widget>[
        Expanded(
          flex: 6,
          child: Container(
            width: size.width,
//            color: Colors.red,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 4,
                  child: Icon(
                    Icons.security,
                    size: size.width * 0.3,
                    color: btnColor,
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    children: <Widget>[
                      Text(
                        pinError ? titleBadPin : title,
                        style: TextStyle(fontSize: 17),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 25),
                        width: (size.width / 8) * 4,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: size.width / 12,
                              height: size.width / 12,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(
                                  color: Colors.black,
                                  width: 2,
                                ),
                                color: pinLegthCount >= 1
                                    ? btnColor
                                    : Colors.transparent,
                              ),
                            ),
                            Container(
                              width: size.width / 12,
                              height: size.width / 12,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(
                                  color: Colors.black,
                                  width: 2,
                                ),
                                color: pinLegthCount >= 2
                                    ? btnColor
                                    : Colors.transparent,
                              ),
                            ),
                            Container(
                              width: size.width / 12,
                              height: size.width / 12,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(
                                  color: Colors.black,
                                  width: 2,
                                ),
                                color: pinLegthCount >= 3
                                    ? btnColor
                                    : Colors.transparent,
                              ),
                            ),
                            Container(
                              width: size.width / 12,
                              height: size.width / 12,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(
                                  color: Colors.black,
                                  width: 2,
                                ),
                                color: pinLegthCount >= 4
                                    ? btnColor
                                    : Colors.transparent,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: Container(
            color: btnColor,
              child: Column(children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                width: size.width,
                color: btnColor,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '1',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Text(
                                  '',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                              ],
                            ),
                            onPressed: () {
                              pressBtn('1');
                            },
                          )),
                    ),
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '2',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Text(
                                  'ABC',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ],
                            ),
                            onPressed: () {
                              pressBtn('2');
                            },
                          )),
                    ),
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '3',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Text(
                                  'DEF',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ],
                            ),
                            onPressed: () {
                              pressBtn('3');
                            },
                          )),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                width: size.width,
                color: btnColor,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '4',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Text(
                                  'GHI',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ],
                            ),
                            onPressed: () {
                              pressBtn('4');
                            },
                          )),
                    ),
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '5',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Text(
                                  'JKL',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ],
                            ),
                            onPressed: () {
                              pressBtn('5');
                            },
                          )),
                    ),
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '6',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Text(
                                  'MNO',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ],
                            ),
                            onPressed: () {
                              pressBtn('6');
                            },
                          )),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                width: size.width,
                color: btnColor,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '7',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Text(
                                  'PQRS',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ],
                            ),
                            onPressed: () {
                              pressBtn('7');
                            },
                          )),
                    ),
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '8',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Text(
                                  'TUV',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ],
                            ),
                            onPressed: () {
                              pressBtn('8');
                            },
                          )),
                    ),
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '9',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Text(
                                  'WXYZ',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ],
                            ),
                            onPressed: () {
                              pressBtn('9');
                            },
                          )),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                width: size.width,
                color: btnColor,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                        height: size.height / 8,
                        child: Container(
                          color: btnColor,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '0',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                              ],
                            ),
                            onPressed: () {
                              pressBtn('0');
                            },
                          )),
                    ),
                    Expanded(
                      flex: 1,
                      child: SizedBox(
                          height: size.height / 8,
                          child: FlatButton(
                            color: btnColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  pinLegthCount == 0
                                      ? Icons.cancel
                                      : Icons.backspace,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                            onPressed: () {
                              setState(() {
                                if (pinLegthCount == 0) {
                                  Navigator.pop(context, false);
                                } else {
                                  pinLegthCount--;
                                  print(_password.substring(0, pinLegthCount));
                                }
                              });
                            },
                          )),
                    )
                  ],
                ),
              ),
            ),
          ])),
        )
      ])),
    ));
  }
}
