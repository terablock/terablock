import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/DashBoard/DashBoardView.dart';
import 'package:TeraBlock/Modules/Sign/In/SignInView.dart';
import 'package:TeraBlock/Modules/TransferDetails/TransferDetailsView.dart';
import 'package:crypto_font_icons/crypto_font_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Future showErrorHttpRequest(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              'Error de conexión',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.signal_cellular_connected_no_internet_4_bar,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(
                  child: Text(
                      'Verifica tu conexión a Internet e intenta de nuevo')),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showEmailRequire(BuildContext context,
    TextEditingController controllerMail, FocusNode _focusNodeMail) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              'Para continuar',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.email,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(
                  child: Text(
                'Se requiere un correo electrónico valido para continuar.',
                textAlign: TextAlign.center,
              )),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    controllerMail.text = '';
                    _focusNodeMail.requestFocus();
                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showEmailUnavailable(BuildContext context,
    TextEditingController controllerMail, FocusNode _focusNodeMail) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              'Correo electronico no disponible',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.email,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(
                  child: Text(
                'El correo electronico ingresado ya se encuentra registrado.',
                textAlign: TextAlign.center,
              )),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    controllerMail.text = '';
                    _focusNodeMail.requestFocus();
                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showSuccessCreateAccount(BuildContext context, String email) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            title: Center(
                child: Text(
              'Cuenta Registrada',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.check_circle_outline,
              size: 70,
              color: Colors.green,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(child: Text('Cuenta:$email activa')),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => SignInView()),
                        (Route<dynamic> route) => false);
                  }),
            ]);
      });
}

Future showSuccessUpdateProfile(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            title: Center(
                child: Text(
              'Perfil Actualizado',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.check_circle_outline,
              size: 70,
              color: Colors.green,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Tu perfil de  usuario terablock se ha actualizado.',
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => DashBoard()),
                        (Route<dynamic> route) => false);
                  }),
            ]);
      });
}

Future showPasswordRequire(BuildContext context,
    TextEditingController controllerPassword, FocusNode _focusNodePassword) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              'Para continuar',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.security,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(
                  child: Text(
                'Se requiere password con una longitud minima de 7 caracteres.',
                textAlign: TextAlign.center,
              )),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    controllerPassword.text = '';
                    _focusNodePassword.requestFocus();
                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showRequireItem(BuildContext context, TextEditingController controller,
    FocusNode _focusNode, String name) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              'Para continuar',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.security,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(
                  child: Text(
                'Se requiere $name para continuar con el proceso de actualización de perfil.',
                textAlign: TextAlign.center,
              )),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    controller.text = '';
                    _focusNode.requestFocus();
                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showPasswordVerify(BuildContext context,
    TextEditingController controllerPassword, FocusNode _focusNodePassword) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              'Para continuar',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.security,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(
                  child: Text(
                'El password de confirmación no coincide al ingresado anteriormente.',
                textAlign: TextAlign.center,
              )),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    controllerPassword.text = '';
                    _focusNodePassword.requestFocus();
                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showSponsorInvalid(BuildContext context, String msj) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              msj,
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.warning,
              size: 70,
              color: Colors.blue,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              new FlatButton(
                  child: Text(
                    'Aceptar',
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future askTermAndConditions(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              'Términos y Condiciones',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.announcement,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              RichText(
                  softWrap: true,
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                            text:
                                'Al presionar el botón aceptar, estas aceptando nuestros '),
                        TextSpan(
                            style: TextStyle(
                                color: Colors.blue,
                                decoration: TextDecoration.underline),
                            text: 'Términos y Condiciones.')
                      ])),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  new FlatButton(
                      child:
                          Text('Cancelar', style: TextStyle(color: Colors.red)),
                      onPressed: () {
                        Navigator.pop(context, false);
                      }),
                  new FlatButton(
                      child: Text(
                        'Aceptar',
                        style: TextStyle(color: Colors.blue),
                      ),
                      onPressed: () {
                        Navigator.pop(context, true);
                      }),
                ],
              )
            ]);
      });
}

Future showErrorWallet(BuildContext context, TextEditingController ctlWallet,
    FocusNode focusNodeWallet) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              ' Wallet ingresado no válido',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.warning,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(
                  child:
                      Text('Verifica el wallet ingresado  e intenta de nuevo')),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    ctlWallet.text = '';
                    focusNodeWallet.requestFocus();

                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showErrorCode(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              ' Codigo ingresado no válido',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.warning,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showErrorNameContact(BuildContext context, TextEditingController ctlName,
    FocusNode focusNodeName) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              ' Nombre ingresado no válido',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.warning,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(
                  child: Text(
                'El nombre debe tener una longitud minima de 5 dígitos y máxima de 25.',
                textAlign: TextAlign.center,
              )),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    ctlName.text = '';
                    focusNodeName.requestFocus();

                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showErrorTypeWallet(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              'Selecciona un tipo Crypto',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.warning,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Text('Debes seleccionar un tipo Crypto.'),
              ),
              SizedBox(
                width: 50,
              ),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showErrorNumberPhoneSignIn(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              'Numero celular invalido',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.warning,
              size: 70,
              color: Colors.blueAccent,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Text('Debes seleccionar un numero celualar valido.'),
              ),
              SizedBox(
                width: 50,
              ),
              new FlatButton(
                  child: Text(
                    'Aceptar',
                    style: TextStyle(color: Colors.blue),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ]);
      });
}

Future showSuccessContact(BuildContext context, String name) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              ' Alta de contacto',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.check,
              size: 70,
              color: Colors.green,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Text('contacto: $name agregado correctamente'),
              ),
              SizedBox(
                width: 50,
              ),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    Navigator.pop(context, true);
                  }),
            ]);
      });
}

Future showSuccessErrorContact(BuildContext context, String name) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Center(
                child: Text(
              ' Alta de contacto',
              textAlign: TextAlign.center,
            )),
            content: Icon(
              Icons.error,
              size: 70,
              color: Colors.red,
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Text(name),
              ),
              SizedBox(
                width: 50,
              ),
              new FlatButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    Navigator.pop(context, true);
                  }),
            ]);
      });
}

Widget showWait() {
  return Expanded(
    flex: 1,
    child: Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        CircularProgressIndicator(),
        SizedBox(
          height: 10,
        ),
        Text('Cargando...'),
      ]),
    ),
  );
}

Widget getIconTransaction(op) {
  switch (op) {
    case '1':
      return Image.asset(
        'assets/Icons/r.png',
        height: 20,
        width: 25,
        color: Colors.white,
      );
      break;
    case '2':
      return Image.asset(
        'assets/Icons/d.png',
        height: 25,
        width: 25,
        color: Colors.white,
      );
    case '3':
      return Image.asset(
        'assets/Icons/d.png',
        height: 25,
        width: 25,
        color: Colors.white,
      );

    default:
      return null;
      break;
  }
}

Widget showHistory(BuildContext context, List<Transfer> transfers) {
  return transfers != null
      ? Expanded(
          child: transfers.length > 0
              ? Card(
                  margin: EdgeInsets.only(top: 10, left: 5, right: 5),
                  child: ListView.builder(
                      itemCount: transfers.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TransferDetails(
                                        transfer: transfers[index],
                                      )),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 124.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                border: Border.all(color: Colors.black12),
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              child: Column(
                                children: [
                                  Expanded(
                                    flex: 5,
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 2,
                                          child: Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            height: MediaQuery.of(context)
                                                .size
                                                .height,
                                            child: Column(children: [
                                              Expanded(
                                                flex: 2,
                                                child: Center(
                                                    child: Text(
                                                  transfers[index]
                                                      .descTransaction,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 15),
                                                )),
                                              ),
                                              Expanded(
                                                  flex: 6,
                                                  child: CircleAvatar(
                                                    radius: 25,
                                                    child: getIconTransaction(
                                                        transfers[index]
                                                            .transaction),
                                                  )),
                                            ]),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 5,
                                          child: Container(
                                            decoration: BoxDecoration(),
                                            child: Column(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 1,
                                                  child: Align(
                                                      alignment:
                                                          Alignment.centerRight,
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                right: 10),
                                                        child: Text(
                                                          transfers[index]
                                                              .createDate,
                                                        ),
                                                      )),
                                                ),
                                                Expanded(
                                                  flex: 5,
                                                  child: Row(
                                                    children: [
                                                      Text(
                                                        transfers[index]
                                                                .typeName,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 25),
                                                      ),
                                                      SizedBox(width: 10),
                                                      Center(
                                                        child: Container(
                                                          child: RichText(
                                                            text: TextSpan(
                                                                text: '\$',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .green,
                                                                    fontSize:
                                                                        25),
                                                                children: [
                                                                  TextSpan(
                                                                    text: transfers[
                                                                            index]
                                                                        .amount,
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .blue,
                                                                        fontSize:
                                                                            25),
                                                                  ),
                                                                ]),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Text(
                                        "hash: 0x${transfers[index].idu.replaceAll('-', '').substring(0,25)}....",
                                        style: TextStyle(color: Colors.green),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                )
              : showNotHistory(),
        )
      : showWait();
}

Widget showNotHistory() {
  return Container(
    alignment: Alignment.center,
//    decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
    child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Icon(
        Icons.report,
        size: 100.0,
        color: Colors.blue,
      ),
      Text(
        'No se encontraron transacciones',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
      ),
    ]),
  );
}

Widget showHomeTransaction(BuildContext context, List<Transfer> transfers) {
  return transfers != null
      ? transfers.length > 0
          ? Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: transfers.length,
                itemBuilder: (context, index) {
                  return Container(
                    color: Colors.white,
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TransferDetails(
                                        transfer: transfers[index],
                                      )),
                            );
                          },
                          child: ListTile(
                            leading: CircleAvatar(
                                backgroundColor: Colors.blue,
                                child: getIconTransaction(
                                    transfers[index].transaction)),
                            subtitle: Text(transfers[index].createDate),
                            title: Text(
                                '${transfers[index].typeName} ---> ${transfers[index].descTransaction}'),
                            trailing: Text(
                              ' \$ ${transfers[index].amount}',
                              style:
                                  TextStyle(color: Colors.green, fontSize: 17),
                            ),
                          ),
                        ),
                        index + 1 < transfers.length ? Divider() : SizedBox(),
                      ],
                    ),
                  );
                },
              ),
            )
          : showNotHistory()
      : showWait();
}

Widget getIcon(op) {
  switch (op) {
    case 3:
      return Image.asset(
        'assets/Icons/usdc.png',
        height: 50,
        width: 50,
      );
      break;
    case 2:
      return Icon(CryptoFontIcons.ETH, size: 50, color: Colors.blueAccent);
      break;
    case 1:
      return Icon(CryptoFontIcons.USDT, size: 50, color: Colors.orange);
      break;
    default:
      return Icon(Icons.error);
      break;
  }
}

Future<bool> showDialogTransaction(
    BuildContext context, Contact contact, String amount, String notes) async {
  return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //t
          title: Text(
            "Verificación de  datos de transacción",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold, decoration: TextDecoration.none),
          ),
          titlePadding: EdgeInsets.symmetric(
            horizontal: 30,
            vertical: 20,
          ),
          children: <Widget>[
            Text(
              "Wallet",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              contact.wallet,
              textAlign: TextAlign.center,
              style:
                  TextStyle(fontWeight: FontWeight.normal, color: Colors.green),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "Monto",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              amount,
              textAlign: TextAlign.center,
              style:
                  TextStyle(fontWeight: FontWeight.normal, color: Colors.green),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "Concepto",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              notes,
              textAlign: TextAlign.center,
              style:
                  TextStyle(fontWeight: FontWeight.normal, color: Colors.green),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "¿Los datos son correctos?",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.normal,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context, false);
                        },
                        child:
                            Text('No', style: TextStyle(color: Colors.black54)),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context, true);
                        },
                        child: Text(
                          'Si',
                          style: TextStyle(color: Colors.blue),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        );
      });
}

Future<bool> verifyNumberPhone(BuildContext context) async {
  String phone = await User().getPhone();

  return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //t
          title: Text(
            "Validación de transacción",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w200),
          ),
          titlePadding: EdgeInsets.symmetric(
            horizontal: 30,
            vertical: 20,
          ),
          children: <Widget>[
            Text(
              phone,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "Enviaremos un código al numero celular registrado",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.normal),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "¿Desea continuar?",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context, false);
                        },
                        child: Text(
                          'No',
                          style: TextStyle(color: Colors.black54),
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context, true);
                        },
                        child: Text(
                          'Si',
                          style: TextStyle(color: Colors.blue),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        );
      });
}

Future<bool> verifyNumberPhoneSignUp(BuildContext context, String phone) async {
  return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //t
          title: Text(
            "Verificación de numero celular",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w200),
          ),
          titlePadding: EdgeInsets.symmetric(
            horizontal: 30,
            vertical: 20,
          ),
          children: <Widget>[
            Text(
              phone,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "Enviaremos un código al numero celular ingresado",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.normal),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "¿Desea continuar?",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context, false);
                        },
                        child:
                            Text('No', style: TextStyle(color: Colors.black54)),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context, true);
                        },
                        child: Text(
                          'Si',
                          style: TextStyle(color: Colors.blue),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        );
      });
}

Future<bool> askVerifyMail(BuildContext context, String mail) async {
  return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //t
          title: Text(
            "Validación de Correo Electronico",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w200),
          ),
          titlePadding: EdgeInsets.symmetric(
            horizontal: 30,
            vertical: 20,
          ),
          children: <Widget>[
            Text(
              mail,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "verifica si el correo electronico esta correctamente escrito.",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.normal),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "¿Desea continuar?",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context, false);
                        },
                        child: Text(
                          'No',
                          style: TextStyle(color: Colors.black54),
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context, true);
                        },
                        child: Text(
                          'Si',
                          style: TextStyle(color: Colors.blue),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        );
      });
}
