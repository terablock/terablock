import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/SignInInfo.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Models/phoneInfo.dart';
import 'package:TeraBlock/Modules/Sign/In/Widgets/DialogCountry.dart';
import 'package:TeraBlock/Modules/Sign/In/Widgets/PhoneTextField.dart';
import 'package:TeraBlock/Modules/Sign/Up/SignUpModel.dart';
import 'package:TeraBlock/Others/FirebaseGlobal.dart';
import 'package:TeraBlock/Others/HexColor.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:flutter/material.dart';

class SignUpView extends StatefulWidget {
  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  SignUpInfo _signUpInfo;
  SignInInfo _signInInfo = new SignInInfo();
  SignUpModule model = new SignUpModule();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  int _currentView = 1;
  int _lastView = 0;
  bool _sponsor = false;
  TextEditingController sponsorController;
  TextEditingController phoneController;

  @override
  void initState() {
    sponsorController = new TextEditingController();
    phoneController = new TextEditingController();
    _signUpInfo = new SignUpInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldkey,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          elevation: 0.0,
          backgroundColor: Colors.white,
//          bottom: PreferredSize(child: Container(color: Colors.orange, height: 4.0,), preferredSize: Size.fromHeight(4.0)),
          title: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Comencemos',
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w300),
              )),
        ),
        body: getView());
  }

  Widget getView() {
    switch (_currentView) {
      case 1:
        return patternVerify();
        break;
      case 2:
        return phoneView();
        break;
      default:
        return noConnexion();
        break;
    }
  }

  Widget noConnexion() {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              'No se puede establecer una conexión con la red',
            ),
            leading: Icon(
              Icons.error,
              color: Colors.red,
            ),
          ),
          RaisedButton(
            child: Text('Reintentar'),
            onPressed: () {
              setState(() {
                if (_lastView == 0) {
                  _currentView = 1;
                } else {
                  _currentView = 2;
                }
              });
            },
          )
        ],
      ),
    );
  }

  Widget patternVerify() {
    return Container(
      margin: EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              height: 20,
              width: MediaQuery.of(context).size.width / 3,
              color:  HexColor('#0F2C45'),
              child: Center(
                  child: Text('1 de 3', style: TextStyle(color: Colors.white))),
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    'Ingresa el pattern de registro',
                    style: TextStyle(fontSize: 17, color: Colors.grey),
                  ),
                ),
                TextField(
                    keyboardType: TextInputType.text,
                    style: Theme.of(context).textTheme.bodyText1,
                    controller: sponsorController,
                    decoration: InputDecoration(
                        prefix: Icon(Icons.security),
                        icon: Icon(Icons.security),
                        labelText: 'Sponsor',
                        errorText: _sponsor ? null : 'Sponsor requerido ',
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)))),
                    onChanged: (val) {
                      setState(() {
                        if (val.length == 0) {
                          _sponsor = false;
                        } else {
                          _sponsor = true;
                        }
                      });
                    }),
              ],
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(0),
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  color: HexColor('#0F2C45'),
                  child: FlatButton(
                    child: Text(
                      'Continuar',
                      style: TextStyle(
                          backgroundColor:  HexColor('#0F2C45'),
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 17),
                    ),
                    onPressed: () async {
                      getProcessFromView();
                    },
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget phoneView() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              height: 20,
              width: (MediaQuery.of(context).size.width / 3) * 2,
              color: HexColor('#0F2C45'),
              child: Center(
                  child: Text('2 de 3', style: TextStyle(color: Colors.white))),
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    '¿Cuál es tu numero celular?',
                    style: TextStyle(fontSize: 17, color: Colors.grey),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 4,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: PhoneTextField(
                          data: _signInInfo,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: GestureDetector(
                          child: _signInInfo.phone.countryImg,
                          onTap: () async {
                            PhoneInfo phone = await dialogCountry(context);
                            setState(() {
                              _signInInfo.phone.dialCode = phone.dialCode;
                              _signInInfo.phone.countryImg = phone.countryImg;
                              _signInInfo.phone.countryName = phone.countryName;
                            });
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(0),
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  color:  HexColor('#0F2C45'),
                  child: FlatButton(
                    child: Text(
                      'Continuar',
                      style: TextStyle(
                          backgroundColor:  HexColor('#0F2C45'),
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 17),
                    ),
                    onPressed: () {
                      getProcessFromView();
                    },
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  void getProcessFromView() {
    switch (_currentView) {
      case 1:
        verifySponsor();
        break;
      case 2:
        verifyPhoneProcess();
        break;
      default:
        break;
    }
  }

  void verifySponsor() async {
    if (!_sponsor) {
      showSponsorInvalid(context, 'Sponsor Invalido');
    } else {
      HttpHandler result =
          await model.verifySponsor(context, sponsorController.text.trim());
      if (result.result) {
        GlobalResult _result = result.data;

        if (_result.result == '1' || _result.result == '2') {
          setState(() {
            _currentView++;
            _signUpInfo.sponsor = sponsorController.text;
            _signUpInfo.typeSponsor = int.parse(_result.result);
          });
        } else {
          showSponsorInvalid(context, _result.msj);
        }
      } else {
        showErrorHttpRequest(context);
      }
    }
  }

  void verifyPhoneProcess() async {
    if (_signInInfo.phone.numberPhone == null) {
      await showErrorNumberPhoneSignIn(context);
    } else {
      HttpHandler result = await model.verifyPhone(
          context, _signInInfo.phone.dialCode + _signInInfo.phone.numberPhone);

      if (result.result) {
        GlobalResult _result = result.data;

        if (_result.result == '1') {
          bool _result = await verifyNumberPhoneSignUp(context,
              _signInInfo.phone.dialCode + ' ' + _signInInfo.phone.numberPhone);
          if (_result) {
            FirebaseGlobal firebaseGlobal = new FirebaseGlobal(
                signUpInfo: _signUpInfo,
                type: 2,
                phoneNumber: _signInInfo.phone.dialCode +
                    _signInInfo.phone.numberPhone
                        .replaceAll(new RegExp(r"\s+\b|\b\s"), ""));
            firebaseGlobal.processPhoneVerification(context, _scaffoldkey);
          }
        } else {
          showSponsorInvalid(context, _result.msj);
        }
      } else {
        showErrorHttpRequest(context);
      }
    }
  }
}
