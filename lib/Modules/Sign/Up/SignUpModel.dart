import 'dart:convert';

import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';

ProgressDialog pr;

class SignUpModule {
  Future<HttpHandler> verifySponsor(
      BuildContext context, String sponsor) async {
    HttpHandler _response = new HttpHandler();

    pr = ProgressDialog(
      context,
      isDismissible: false,
    );

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    try {
      final response = await http
          .get('https://terablock.app/terablock/api/sponsor/$sponsor')
          .timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = GlobalResult.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';
    }
    await pr.hide();
    return _response;
  }


  Future<HttpHandler> verifyPhone(BuildContext context,String phone) async{
    HttpHandler _response = new HttpHandler();

    pr = ProgressDialog(
      context,
      isDismissible: false,
    );

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    try {
      final response = await http
          .post('https://terablock.app/terablock/api/verifyphone',
          body: jsonEncode(<String, String>{
          'phone':phone.replaceAll(new RegExp(r"\s+\b|\b\s"), "")})
      ).timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = GlobalResult.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';

    }
    await pr.hide();
    return _response;
  }


  Future<HttpHandler> registerUser(BuildContext context,SignUpInfo signUpInfo) async{
    HttpHandler _response = new HttpHandler();

    pr = ProgressDialog(
      context,
      isDismissible: false,
    );

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    try {
      final response = await http
          .post('https://terablock.app/terablock/api/users',
          body: jsonEncode(<String, String>{
            'mail':signUpInfo.mail,
            'phoneNumber':signUpInfo.phoneNumber.replaceAll(new RegExp(r"\s+\b|\b\s"), ""),
            'password':signUpInfo.password,
            'sponsor':signUpInfo.sponsor,
            'typeSponsor':signUpInfo.typeSponsor.toString()
          })

      ).timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = GlobalResult.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';

    }
    await pr.hide();
    return _response;
  }

}
