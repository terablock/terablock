import 'package:flag/flag.dart';

class PhoneInfo{
  String dialCode = '+52';
  String countryName = 'México';
  String numberPhone;
  Flag countryImg = Flag('MX',width: 40,height: 30,);
}