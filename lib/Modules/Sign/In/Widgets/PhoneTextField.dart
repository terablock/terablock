import 'package:TeraBlock/Others/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:TeraBlock/Models/SignInInfo.dart';
import 'package:TeraBlock/others/NumberTextInputFormatter.dart';

class PhoneTextField extends StatefulWidget {
  final SignInInfo data;
  PhoneTextField({this.data});

  @override
  _PhoneTextFieldState createState() => _PhoneTextFieldState();
}

class _PhoneTextFieldState extends State<PhoneTextField> {
  final _mobileFormatter = NumberTextInputFormatter();
  bool _phoneError = false;
  @override
  Widget build(BuildContext context) {
    return TextField(
      inputFormatters: <TextInputFormatter>[
        WhitelistingTextInputFormatter.digitsOnly,
        _mobileFormatter,
      ],
      keyboardType: TextInputType.phone,
      style: TextStyle(fontSize: 17, color: Colors.grey),
      decoration: InputDecoration(
        icon: Icon(
          Icons.phone_iphone,
          size: 30.0,
          color:  HexColor('#0F2C45'),
        ),
//                      errorText: 'jesus',
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
        errorText: _phoneError?'Numero Celular Invalido':null,
        hintText: 'Celular',
      ),
      onTap: () async {

      },
      onChanged: (val) {
        setState(() {
          if (val.trim().length ==13 ||val.trim().length == 10) {
            _phoneError = false;
            widget.data.phone.numberPhone = val;
          } else {
            _phoneError = true;
            widget.data.phone.numberPhone = null;

          }
        });
      },
    );
  }
}
