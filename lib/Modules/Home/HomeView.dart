import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/Home/HomeModel.dart';
import 'package:TeraBlock/Modules/Profile/EditProfileView.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:flutter/material.dart';
import '../../navigation_bloc.dart';
import 'package:intl/intl.dart';

final formatCurrency = new NumberFormat.simpleCurrency();

class HomePage extends StatefulWidget with NavigationStates {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  UserBalance _user = new UserBalance();
  bool internetConexion = true;
  HomeModel model = new HomeModel();
  @override
  void initState() {
    load();
    super.initState();
  }

  void load() async {
    HttpHandler _response = await model.getBalance(context);
    if (_response.result) {
      setState(() {
        internetConexion = true;
        _user = _response.data;
      });
    } else {
      setState(() {
        internetConexion = false;
      });

      showErrorHttpRequest(context);
    }
  }

  Widget noConexion() {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              'No se puede establecer una conexión con la red',
            ),
            leading: Icon(
              Icons.error,
              color: Colors.red,
            ),
          ),
          RaisedButton(
            child: Text('Reintentar'),
            onPressed: () {
              load();
            },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return internetConexion
        ? Container(
            child: Column(
              children: <Widget>[
                _user.balance != null
                    ? AvailableAmountView(
                        amount: _user.balance,
                      )
                    : showWait(),
                Container(
                  height: 40,
                  padding: EdgeInsets.only(left: 15),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Hoy',
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                if (_user.profileStatus == '0')
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: (MediaQuery.of(context).size.height / 3),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          children: <Widget>[
                            Align(
                                alignment: Alignment.topLeft,
                                child: CircleAvatar(child: Icon(Icons.person))),
                            Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  'Favor de completar su perfil de usuario',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 22),
                                )),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Es necesario contar con la información que se solicita para cualquier aclaración',
                                style: TextStyle(fontSize: 17),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 1,
                              height: 50,
                              child: RaisedButton(
                                  color: Colors.blue,
                                  child: Text(
                                    'Completar perfil',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 17),
                                  ),
                                  onPressed: () async {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => EditProfileView(
                                              profileInfo: null)),
                                    );
                                  }),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                SizedBox(
                  height: 10,
                ),
                showHomeTransaction(context, _user.transfers)
//
              ],
            ),
          )
        : noConexion();
  }
}

class AvailableAmountView extends StatelessWidget {
  final String amount;

  AvailableAmountView({this.amount});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20),
      height: 120,
      color: Colors.white,
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Text(
            'Activos disponibles',
            style: TextStyle(color: Colors.grey, fontSize: 15),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
              NumberFormat.currency().format(double.parse(amount)),
            //'\$$amount',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
          )
        ],
      ),
    );
  }
}
