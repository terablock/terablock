import 'package:flag/flag.dart';
import 'package:flutter/material.dart';
import 'package:TeraBlock/Models/phoneInfo.dart';

Future<PhoneInfo> dialogCountry(BuildContext context) {
  PhoneInfo phone = new PhoneInfo();

  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
          title: Center(
              child: Text(
            'Selecciona tu País',
            textAlign: TextAlign.center,
          )),
          content: Container(
            height: 300,
            child: Column(children: [
              Divider(
                height: 20,
                color: Colors.black,
              ),
              Divider(),
              GestureDetector(
                onTap: () {
                  phone.dialCode = '+52';
                  phone.countryName = 'México';
                  phone.countryImg = Flag('MX',width: 40,height: 30,);
                  Navigator.pop(context, phone);
                },
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flag(
                        'MX',
                        width: 50,
                        height: 50,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        'México',
                        style: TextStyle(fontSize: 20),
                      ),
                    ]),
              ),
              Divider(),
              GestureDetector(
                onTap: () {
                  phone.dialCode = '+1';
                  phone.countryName = 'United States';
                  phone.countryImg = Flag('US',width: 40,height: 30,);
                  Navigator.pop(context, phone);
                },
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flag(
                        'US',
                        width: 50,
                        height: 50,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        'United States',
                        style: TextStyle(fontSize: 20),
                      ),
                    ]),
              ),
              Divider(),
              GestureDetector(
                onTap: () {
                  phone.dialCode = '+1';
                  phone.countryName = 'Canada';
                  phone.countryImg = Flag('CA',width: 40,height: 30,);
                  Navigator.pop(context, phone);
                },
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flag(
                        'CA',
                        width: 50,
                        height: 50,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        'Canada',
                        style: TextStyle(fontSize: 20),
                      ),
                    ]),
              )
            ]),
          ),
          contentPadding: EdgeInsets.all(10.0),
        );
      });
}
