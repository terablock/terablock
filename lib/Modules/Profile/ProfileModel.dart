import 'dart:convert';

import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
ProgressDialog pr;

class ProfileModel{
  Future<HttpHandler> getProfile(BuildContext context) async {

    HttpHandler _response = new HttpHandler();
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );
    String userID = await User().getUserID();

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    try {
      final response = await http
          .get('https://terablock.app/terablock/api/user/profile/$userID')
          .timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = ProfileInfo.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';
    }
    await pr.hide();
    return _response;
  }

  Future<HttpHandler> updateProfile(BuildContext context,ProfileInfo profile) async {

    HttpHandler _response = new HttpHandler();
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );
    String userID = await User().getUserID();

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    try {
      final response = await http
          .put('https://terablock.app/terablock/api/user/profile',body: jsonEncode(<String, String>{
        'idu':userID,
        'names':profile.name,
        'fatherName':profile.fatherSurname,
        'motherName':profile.motherSurname.trim().length==0?' ':profile.motherSurname,
        'brithDay':profile.birthDate,
        'country':profile.country,
        'state':profile.state,
        'city':profile.city,
        'zipCode':profile.zipCode
      }))
          .timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = GlobalResult.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';
    }
    await pr.hide();
    return _response;
  }



}