import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/Notification/Notifications.dart';
//import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:TeraBlock/modules/Settings/SettingsView.dart';
import 'package:TeraBlock/navigation_bloc.dart';

var notificationsReference;

class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  MyAppBar({Key key})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);


  @override
  final Size preferredSize; // default is 56.0

  @override
  _MyAppBarState createState() => _MyAppBarState();
}

class _MyAppBarState extends State<MyAppBar> {
  int _counter = 0;
  List<NotificationGlobal> items;
//  StreamSubscription<Event> _onNotAddedSubscription;
//  StreamSubscription<Event> _onNotChangedSubscription;
//  StreamSubscription<Event> _onNotDeleteSubscription;

  @override
  void initState() {
    super.initState();
//    getNotifications();
  }


  @override
  void dispose() {
//    _onNotAddedSubscription.cancel();
//    _onNotChangedSubscription.cancel();
//    _onNotDeleteSubscription.cancel();
    super.dispose();
  }
//
//  getNotifications() async {
//   String _user =  await User().getUserID();
//    notificationsReference = FirebaseDatabase.instance
//        .reference()
//        .child('notification')
//        .child(_user);
//
//    items = new List();
//
//    _onNotAddedSubscription =
//        notificationsReference.onChildAdded.listen(_onNoteAdded);
//    _onNotChangedSubscription =
//        notificationsReference.onChildChanged.listen(_onNoteUpdated);
//    _onNotDeleteSubscription =
//        notificationsReference.onChildRemoved.listen(_deleteNote);
//  }

//  void _onNoteAdded(Event event) {
//    setState(() {
//      items.add(new NotificationGlobal.fromSnapshot(event.snapshot));
//      _counter = items.length;
//    });
//  }

//  void _onNoteUpdated(Event event) {
//    var oldNoteValue =
//        items.singleWhere((note) => note.id == event.snapshot.key);
//    setState(() {
//      items[items.indexOf(oldNoteValue)] =
//          new NotificationGlobal.fromSnapshot(event.snapshot);
//      _counter = items.length;
//    });
//  }

//  void _deleteNote(Event event) async {
//    print(event.snapshot.key);
//    items.removeWhere((item) =>
//        item.id == NotificationGlobal.fromSnapshot(event.snapshot).id);
//    setState(() {
//      _counter = items.length;
//    });
//  }

  Widget myAppBarIcon() {
    return Container(
      width: 30,
      height: 30,
      child: Stack(
        children: [
          Icon(
            Icons.notifications_none,
            color: Colors.black,
            size: 30,
          ),
          Container(
            width: 30,
            height: 30,
            alignment: Alignment.topRight,
            margin: EdgeInsets.only(top: 5),
            child: Container(
              width: 15,
              height: 15,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xffc32c37),
                  border: Border.all(color: Colors.white, width: 1)),
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Center(
                  child: Text(
                    _counter.toString(),
                    style: TextStyle(fontSize: 10),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Center(
        child: Text(
          'TeraBlock',
          style: TextStyle(color: Colors.black),
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.black, //change your color here
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      leading: Padding(
        padding: EdgeInsets.only(left: 12),
        child: IconButton(
          icon: Icon(
            Icons.person_pin,
            size: 40,
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SettingsView()),
            );
          },
        ),
      ),
      actions: <Widget>[
//        Future implementation notificationes and search
//        IconButton(
//          icon: Icon(
//            Icons.search,
//            size: 30,
//          ),
//          onPressed: () {
//            print('Click search');
//          },
//        ),
       if(_counter>0) IconButton(
          icon: myAppBarIcon(),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => NotificationsView(
                        notifications: items ?? null,
                      )),
            );
          },
        ),
      ],
    );
  }
}

class MyBottomNavigationBar extends StatefulWidget {
  final BuildContext myContext;

  MyBottomNavigationBar(this.myContext);

  @override
  _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    BlocProvider.of<NavigationBloc>(context)
        .add(NavigationEvents.values[index]);
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Home'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.history),
          title: Text('Historial'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.send),
          title: Text('Enviar'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.get_app),
          title: Text('Recivir'),
        ),
      ],
      type: BottomNavigationBarType.fixed,
      showUnselectedLabels: true,
      showSelectedLabels: true,
      currentIndex: _selectedIndex,
      unselectedLabelStyle: TextStyle(color: Colors.grey),
      selectedItemColor: Colors.blueAccent,
      selectedIconTheme: IconThemeData(color: Colors.blueAccent),
      unselectedIconTheme: IconThemeData(color: Colors.black),
      selectedLabelStyle: TextStyle(color: Colors.black),
      onTap: _onItemTapped,
    );
  }
}
