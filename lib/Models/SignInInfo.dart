import 'package:flutter/cupertino.dart';
import 'package:TeraBlock/Models/phoneInfo.dart';

class SignInInfo {
  String email;
  PhoneInfo phone;
  String password;
  SignInControllers controllers = new SignInControllers();
  SignInFocusNodes focusNodes = new SignInFocusNodes();

  SignInInfo() {
    phone = new PhoneInfo();
  }

  dispose(){
    controllers.emailController.dispose();
    controllers.phoneController.dispose();
    controllers.passwordController.dispose();
    focusNodes.emailFocusNode.dispose();
    focusNodes.phoneFocusNode.dispose();
    focusNodes.passwordFocusNode.dispose();
  }
}

class SignInControllers {
  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
}

class SignInFocusNodes {
  FocusNode emailFocusNode = new FocusNode();
  FocusNode phoneFocusNode = new FocusNode();
  FocusNode passwordFocusNode = new FocusNode();
}
