import 'dart:io';

import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/Sign/Up/RegisterUser.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

enum authProblems { UserNotFound, smsCodeNotValid, NetworkError }

class FirebaseGlobal {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  SignUpInfo signUpInfo;
  String verificationId = '';
  String phoneNumber = '';
  String smsCode = '';
  int type = 1;
  final Contact contact;
  final String note;
  ProgressDialog pr;

  FirebaseGlobal(
      {this.contact, this.note, this.phoneNumber, this.type, this.signUpInfo});

  void processPhoneVerification(
      BuildContext context, GlobalKey<ScaffoldState> _scaffoldkey) async {
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    _verifyPhoneNumber(context, _scaffoldkey);
  }

  _verifyPhoneNumber(
      BuildContext context, GlobalKey<ScaffoldState> _scaffoldkey) async {
    if (type == 1)
      phoneNumber = await User().getPhone();
    else
      signUpInfo.phoneNumber =
          phoneNumber.replaceAll(new RegExp(r"\s+\b|\b\s"), "");

    await FirebaseAuth.instance.signOut();

    await _auth.verifyPhoneNumber(
        phoneNumber: phoneNumber.replaceAll(new RegExp(r"\s+\b|\b\s"), ""),
        timeout: Duration(seconds: 5),
        verificationCompleted: (authCredential) =>
            _verificationComplete(authCredential, context, _scaffoldkey),
        verificationFailed: (authException) =>
            _verificationFailed(authException, context, _scaffoldkey),
        codeAutoRetrievalTimeout: (verificationId) =>
            _codeAutoRetrievalTimeout(context, verificationId, _scaffoldkey),
        codeSent: (verificationId, [code]) =>
            _smsCodeSent(verificationId, [code]));
  }

  _verificationComplete(AuthCredential authCredential, BuildContext context,
      GlobalKey<ScaffoldState> _scaffoldkey) {
    FirebaseAuth.instance
        .signInWithCredential(authCredential)
        .then((authResult) {
      pr.hide();

      processEnd(context);
    });
  }

  _smsCodeSent(String _verificationId, List<int> code) {
    verificationId = _verificationId;
  }

  _verificationFailed(AuthException authException, BuildContext context,
      GlobalKey<ScaffoldState> _scaffoldkey) {
    pr.hide();
    showErrorHttpRequest(context);
    showErrorVerifyPhone(_scaffoldkey);
  }

  _codeAutoRetrievalTimeout(BuildContext context, String _verificationId,
      GlobalKey<ScaffoldState> _scaffoldkey) {
    verificationId = _verificationId;
    smsCodeDialog(context, _scaffoldkey);
  }

  Future smsCodeDialog(
      BuildContext context, GlobalKey<ScaffoldState> _scaffoldkey) async {
    pr.hide();
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext _context) {
          return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //t
            title: Text('Ingresa código de verificación'),
            content: TextField(
              keyboardType: TextInputType.number,
              onChanged: (value) {
                smsCode = value;
              },
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new FlatButton(
                      child: Text(
                        'Cancelar',
                        style: TextStyle(color: Colors.red),
                      ),
                      onPressed: () {
                        Navigator.pop(_context, false);
//                  _verifyPhoneNumber(_context,_scaffoldkey);
//                  processPhoneVerification(context, _scaffoldkey);
                      },
                    ),
                    new FlatButton(
                      child: Text('Reenviar código',
                          style: TextStyle(color: Colors.deepPurpleAccent)),
                      onPressed: () {
                        Navigator.pop(_context);
//                  _verifyPhoneNumber(_context,_scaffoldkey);
                        processPhoneVerification(context, _scaffoldkey);
                      },
                    ),
                    new FlatButton(
                      child: Text('Aceptar'),
                      onPressed: () {
                        Navigator.pop(_context, true);
                        pr.show();
                        signInWithPhoneNumber(_scaffoldkey, context);
                      },
                    )
                  ],
                ),
              ),
            ],
          );
        });
  }

  void signInWithPhoneNumber(
      GlobalKey<ScaffoldState> _scaffoldkey, BuildContext context) async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: verificationId,
      smsCode: smsCode,
    );

    try {
      await _auth
          .signInWithCredential(credential)
          .then((credentials) => processEnd(context))
          .timeout(Duration(seconds: 5));
    } catch (e) {
      pr.hide();
      authProblems errorType;
      if (Platform.isAndroid) {
        switch (e.message) {
          case 'There is no user record corresponding to this identifier. The user may have been deleted.':
            errorType = authProblems.UserNotFound;
            break;
          case 'The sms verification code used to create the phone auth credential is invalid. Please resend the verification code sms and be sure use the verification code provided by the user.':
            errorType = authProblems.smsCodeNotValid;
            showWrongCode(_scaffoldkey);
            break;
          case 'A network error (such as timeout, interrupted connection or unreachable host) has occurred.':
            errorType = authProblems.NetworkError;
            break;
          case 'Cannot create PhoneAuthCredential without either verificationProof, sessionInfo, temporary proof, or enrollment ID.':
            break;
          // ...
          default:
            {
              showErrorHttpRequest(context);
            }
        }
      } else if (Platform.isIOS) {
        switch (e.code) {
          case 'Error 17011':
            errorType = authProblems.UserNotFound;
            break;
          case 'Error 17009':
            errorType = authProblems.smsCodeNotValid;
            break;
          case 'Error 17020':
            errorType = authProblems.NetworkError;
            break;
          default:
            print('Case ${e.message} is not yet implemented');
        }
      }
      print('The error is $errorType');
    }
  }

  void showErrorVerifyPhone(GlobalKey<ScaffoldState> _scaffoldkey) {
    pr.hide();
    final snackBar = SnackBar(
      content: Text('Favor de intentar de nuevo'),
      duration: Duration(seconds: 1),
    );
    _scaffoldkey.currentState.showSnackBar(snackBar);
  }

  void showWrongCode(GlobalKey<ScaffoldState> _scaffoldkey) async {
    await pr.hide();
    final snackBar = SnackBar(
      content: Text('El codigo ingresado es incorrecto, intente de nuevo'),
      backgroundColor: Colors.red,
      action: SnackBarAction(
        label: 'Aceptar',
        onPressed: () {},
      ),
    );
    _scaffoldkey.currentState.showSnackBar(snackBar);
  }

  processEnd(BuildContext context) async {
    switch (type) {
      case 2:
        await pr.hide();

        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => RegisterUser(
                    signUpInfo: signUpInfo,
                  )),
        );
        break;
    }
  }
}
