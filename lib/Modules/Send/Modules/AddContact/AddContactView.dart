import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/Send/SendModel.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:barcode_scan/gen/protos/protos.pb.dart';
import 'package:barcode_scan/model/android_options.dart';
import 'package:barcode_scan/model/scan_options.dart';
import 'package:barcode_scan/platform_wrapper.dart';
import 'package:ethereum_address/ethereum_address.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class AddContactView extends StatefulWidget {
  @override
  _AddContactViewState createState() => _AddContactViewState();
}

class _AddContactViewState extends State<AddContactView> {
  bool internetConnexion = true;
  SendModel model = new SendModel();
  CryptoTypes _cryptoTypes;
  String _currentSelectedItem;
  TextEditingController ctlWallet = new TextEditingController();
  TextEditingController ctlName = new TextEditingController();
  FocusNode _focusNodeWallet = FocusNode();
  FocusNode _focusNodeName = FocusNode();

  ScanResult scanResult;

  final _flashOnController = TextEditingController(text: "Flash on");
  final _flashOffController = TextEditingController(text: "Flash off");
  final _cancelController = TextEditingController(text: "Cancel");

  var _aspectTolerance = 0.00;
  var _selectedCamera = -1;
  var _useAutoFocus = true;
  var _autoEnableFlash = false;

  static final _possibleFormats = BarcodeFormat.values.toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);

  List<BarcodeFormat> selectedFormats = [..._possibleFormats];


  @override
  void initState() {
    getCryptoTypes();
    super.initState();

  }

  void getCryptoTypes() async {
    HttpHandler _response = await model.getCryptos(context);

    if (_response.result) {
      _cryptoTypes = _response.data;
      setState(() {
        internetConnexion = true;
      });
    } else {
      showErrorHttpRequest(context);
      setState(() {
        internetConnexion = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Alta de Contacto',
            style: TextStyle(color: Colors.black),
          ),
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.white,
        ),
        body: Container(
          child: Column(
            children: <Widget>[getView()],
          ),
        ));
  }

  Widget getView() {
    if (!internetConnexion) {
      return noConnexion();
    } else if (_cryptoTypes == null) {
      return showWait();
    } else {
      return formNewContact();
    }
  }

  Widget formNewContact() {
    return Expanded(
      child: ListView(
        padding: EdgeInsets.only(left: 10, right: 10),
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('Tipo Crypto'),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  border: Border.all(
                      color: Colors.grey,
                      style: BorderStyle.solid,
                      width: 0.80),
                ),
                child: DropdownButton(
                  icon: Icon(Icons.arrow_downward),
                  hint: Text('Selecciona tipo Crypto'),
                  isExpanded: true,
                  items: _cryptoTypes.cryptoType.map(
                    (val) {
                      return DropdownMenuItem(
                        value: val.idu,
                        child: Text(val.name),
                      );
                    },
                  ).toList(),
                  value: _currentSelectedItem,
                  onChanged: (value) {
                    setState(() {
                      print(value);
                      _currentSelectedItem = value;
                    });
                  },
                ),
              ),
              Divider(),
              Text('Wallet'),
              TextField(
                  focusNode: _focusNodeWallet,
                  controller: ctlWallet,
                  keyboardType: TextInputType.emailAddress,
                  autofocus: false,
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(Icons.camera_alt),

                        onPressed: () async {
                          scan();
                        },
                      ),
                      labelText: 'Ingresa Wallet',
                      border: OutlineInputBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0)))),
                  onChanged: (val) {
//
                  }),
              Divider(),
              Text('Nombre'),
              TextField(
                  focusNode: _focusNodeName,
                  controller: ctlName,
                  keyboardType: TextInputType.emailAddress,
                  autofocus: false,
                  style: Theme.of(context).textTheme.bodyText1,
                  maxLength: 25,
                  decoration: InputDecoration(
                      labelText: 'Ingresa Nombre',
                      border: OutlineInputBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0)))),
                  onChanged: (val) {}),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 15, right: 15),
                child: Center(
                  child: Align(
                    alignment: Alignment.center,
                    child: ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width,
                      height: 50,
                      child: RaisedButton(
                        color: Colors.blue,
                        shape: ContinuousRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0)),
                        child: Text(
                          "Guardar",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () => saveContact(),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  saveContact() async {
    bool isValidWallet = isValidEthereumAddress(ctlWallet.text); //42

    if (!isValidWallet) {
      showErrorWallet(context,ctlWallet,_focusNodeWallet);
    } else if (ctlName.text.length < 5) {
      showErrorNameContact(context,ctlName,_focusNodeName);
    }
    else if(_currentSelectedItem==null) {
      showErrorTypeWallet(context);
    }
    else{
        Contact contact = new Contact(name: ctlName.text,type:int.parse(_currentSelectedItem),wallet: ctlWallet.text);

     HttpHandler _result =  await model.addContact(context, contact);

     if(_result.result){
       GlobalResult result = _result.data;

       if(result.result == '1'){
        await showSuccessContact(context,ctlName.text);
        Navigator.pop(context,true);
       }else{
         showSuccessErrorContact(context,result.msj);
       }
       print(result);
     }else{
       showErrorHttpRequest(context);
     }
    }
  }

  Widget noConnexion() {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              'No se puede establecer una conexión con la red',
            ),
            leading: Icon(
              Icons.error,
              color: Colors.red,
            ),
          ),
          RaisedButton(
            child: Text('Reintentar'),
            onPressed: () => getCryptoTypes(),
          )
        ],
      ),
    );
  }

  Future scan() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": _cancelController.text,
          "flash_on": _flashOnController.text,
          "flash_off": _flashOffController.text,
        },
        restrictFormat: selectedFormats,
        useCamera: _selectedCamera,
        autoEnableFlash: _autoEnableFlash,
        android: AndroidOptions(
          aspectTolerance: _aspectTolerance,
          useAutoFocus: _useAutoFocus,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);
      setState(() {
        ctlWallet.text = result.rawContent;
      });


    } on PlatformException catch (e) {

      if (e.code == BarcodeScanner.cameraAccessDenied) {
//        setState(() {
          print('The user did not grant the camera permission!');
//        });
      }

    }
  }

}
