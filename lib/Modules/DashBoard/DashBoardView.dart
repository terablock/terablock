import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:TeraBlock/modules/DashBoard/Widgtes/WidgetsRepo.dart';
import 'package:TeraBlock/navigation_bloc.dart';

class DashBoard extends StatefulWidget {
  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return BlocProvider<NavigationBloc>(
        create: (context) => NavigationBloc(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: Scaffold(
            appBar: MyAppBar(),
            body: Column(
              children: <Widget>[
                Expanded(
                  child: BlocBuilder<NavigationBloc, NavigationStates>(
                    builder: (context, navigationState) {
                      return navigationState as Widget;
                    },
                  ),
                ),
              ],
            ),
            bottomNavigationBar: MyBottomNavigationBar(context),
          ),
        ));
  }
}
