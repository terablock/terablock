import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NumberTextInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    final int newTextLength = newValue.text.trim().length;
    int selectionIndex = newValue.selection.end;
    final StringBuffer newText = new StringBuffer();

    if (newTextLength == 10 ) {
      newText.write(newValue.text.trim().substring(0, 3) +' '
          +newValue.text.trim().substring(3, 6)+ ' '
          +newValue.text.trim().substring(6, 8)+ ' '
          +newValue.text.trim().substring(8, 10)+ ' ');
      selectionIndex += 3;
    }else if(newTextLength>10){
      newText.write(oldValue.text);
      selectionIndex = oldValue.text.length-1;
    }else{
      newText.write(newValue.text.trim());

    }
    return new TextEditingValue(
      text: newText.toString(),
      selection: new TextSelection.collapsed(offset: selectionIndex),
    );
  }
}