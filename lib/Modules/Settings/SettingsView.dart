import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/GlobalPin/PinView.dart';
import 'package:TeraBlock/Modules/Home/HomeModel.dart';
import 'package:TeraBlock/Modules/Profile/ProfileView.dart';
import 'package:TeraBlock/Modules/Welcome/WelcomeView.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsView extends StatefulWidget {
  @override
  _SettingsViewState createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  bool pin = false;
  bool internetConexion = true;
  HomeModel model = new HomeModel();
  UserBalance userBalance;
  bool _switchValue = false;

  @override
  void initState() {
    getStatusPin();
    getBalance();
    super.initState();
  }

  getBalance() async {
    HttpHandler _response = await model.getBalance(context);
    if (_response.result) {
      setState(() {
        internetConexion = true;
        userBalance = _response.data;
        print(userBalance.profileStatus);
      });
    } else {
      setState(() {
        internetConexion = false;
      });

      showErrorHttpRequest(context);
    }
  }

  getStatusPin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _switchValue = prefs.getBool('pin') ?? false;
    });
  }

  changePin(bool value) async {
    if (value) {
      bool result = await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => GlobalPin(type: 1)),
      );

      if (result) {
        setState(() {
          getStatusPin();
        });
      }
    } else {
      bool result = await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => GlobalPin(type: 0)),
      );

      if (result) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.remove('pin');
        getStatusPin();
      }
    }
  }

  Widget getView() {
    if (!internetConexion) {
      return noConnexion();
    } else if (userBalance == null) {
      return Container(child: Column(children: [showWait()]));
    } else {
      return settingForm();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Ajustes',
          style: TextStyle(color: Colors.black),
        ),
        elevation: 0.0,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
      ),
      body: getView(),
    );
  }

  Widget settingForm() {
    return Container(
      child: SingleChildScrollView(
        child: userBalance == null
            ? SizedBox()
            : Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 15),
                    alignment: Alignment.centerLeft,
                    height: 60,
                    child: Text(
                      'General',
                      style: TextStyle(
                          fontSize: 17,
                          color: Colors.grey,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  if(userBalance.profileStatus == '1')  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProfileView(
                            )),
                      );
                    },
                    child: Container(
                      padding: EdgeInsets.only(bottom: 20),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 0.0),
                              child: Icon(
                                Icons.person,
                                color: Colors.black45,
                                size: 28,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 5,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Text(

                                   'Perfil',
                                style: TextStyle(fontSize: 17),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: Icon(
                              Icons.lock,
                              color: Colors.black45,
                              size: 28,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'NIP',
                                  style: TextStyle(fontSize: 17),
                                ),
                                Container(
                                  margin: EdgeInsets.only(right: 20),
                                  child: CupertinoSwitch(
                                    value: _switchValue,
                                    onChanged: (value) {
                                      changePin(value);
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Divider(),
                  Container(
                    padding: EdgeInsets.only(left: 15),
                    alignment: Alignment.centerLeft,
                    height: 60,
                    child: Text(
                      'Legal',
                      style: TextStyle(
                          fontSize: 17,
                          color: Colors.grey,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: Icon(
                              Icons.insert_drive_file,
                              color: Colors.black45,
                              size: 28,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              'Términos y condiciones',
                              style: TextStyle(fontSize: 17),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: Icon(
                              Icons.insert_drive_file,
                              color: Colors.black45,
                              size: 28,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              'Políticas de privacidad',
                              style: TextStyle(fontSize: 17),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Divider(),
                  Container(
                    padding: EdgeInsets.only(left: 15),
                    alignment: Alignment.centerLeft,
                    height: 60,
                    child: Text(
                      'Legal',
                      style: TextStyle(
                          fontSize: 17,
                          color: Colors.grey,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0.0),
                            child: Icon(
                              CupertinoIcons.mail_solid,
                              color: Colors.black45,
                              size: 28,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              'Contacto',
                              style: TextStyle(fontSize: 17),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
//                padding: EdgeInsets.only(left: 20,right: 20),
                    width: MediaQuery.of(context).size.width - 40,
                    height: 50,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.red,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: FlatButton(
//                  color: Colors.red,
                      child: Text(
                        'Cerrar Session',
                        style: TextStyle(color: Colors.red, fontSize: 17),
                      ),
                      onPressed: () async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        prefs.remove('user');

                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) => WelcomeView()),
                            (Route<dynamic> route) => false);
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 30),
                    child: Text('Copyright \u00a9 2020 TeraBlock'),
                  )
                ],
              ),
      ),
    );
  }

  Widget noConnexion() {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              'No se puede establecer una conexión con la red',
            ),
            leading: Icon(
              Icons.error,
              color: Colors.red,
            ),
          ),
          RaisedButton(
            child: Text('Reintentar'),
            onPressed: () {
              getBalance();
            },
          )
        ],
      ),
    );
  }
}
