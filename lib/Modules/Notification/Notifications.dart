import 'package:TeraBlock/Models/User.dart';
import 'package:flutter/material.dart';

class NotificationsView extends StatelessWidget {
  NotificationsView({this.notifications});
  final List<NotificationGlobal> notifications;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Notificaciones',
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        margin: EdgeInsets.all(10),
        child: Column(children: <Widget>[
          Expanded(
            child: ListView.builder(
                itemCount: notifications.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  return GestureDetector(
                    onLongPress: (){
                      print('mostrar opcion');
                    },
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 9,
                        child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.stretch,  // add th
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 15),
                                    child: Text(
                                      notifications[index].title,
//                                        textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 17),
                                    ),
                                  ),
                                ),
                                new Container(
                                  padding: new EdgeInsets.only(
                                      left: 15, right: 15),
                                  child: new Text(
                                    notifications[index].description,
                                    textAlign: TextAlign.justify,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 3,
                                    softWrap: true,
                                    style: new TextStyle(
                                      fontSize: 12.0,
                                      height: 1,
//                                          fontFamily: 'Roboto',
//                                          color: new Color(0xFF212121),
//                                            fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Divider()
                          ],
                        )
//                    child:
                        ),
                  );
                }),
          ),
        ]),
      ),
    );
  }
}
