import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/SignInInfo.dart';
import 'package:http/http.dart' as http;
import 'package:TeraBlock/modules/Sign/In/Models/login.dart';

ProgressDialog pr;

class SignInModel {
  Future<HttpHandler> signIn(SignInInfo data, BuildContext context) async{
    HttpHandler _response = new HttpHandler();
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );

    pr.style(
      message: 'Iniciando Sesión',
    );

    await pr.show();
    try {
      final response = await http
          .post('https://terablock.app/terablock/api/login',
          body: jsonEncode(<String, String>{
        'mail':data.email,
        'phone_number':data.phone.dialCode  + data.phone.numberPhone.replaceAll(new RegExp(r"\s+\b|\b\s"), ""),
        'password': data.password
      })).timeout(Duration(seconds: 3));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = Login.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'error when exec function login(model)';
    }

    await pr.hide();
    return _response;
  }


 void saveSession(String iduUser,SignInInfo _signInInfo)async{
    List<String> userData = new List<String>();
    userData.add(iduUser);
    userData.add(_signInInfo.phone.dialCode+_signInInfo.phone.numberPhone);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('user', userData);
  }
}
