import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/receive/ReceiveModule.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:TeraBlock/navigation_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';

class ReceiveView extends StatefulWidget with NavigationStates {
  @override
  _ReceiveViewState createState() => _ReceiveViewState();
}

class _ReceiveViewState extends State<ReceiveView> {
  ReceiveModule model = new ReceiveModule();
  bool internetConnexion = true;
  Wallet myWallet;
  @override
  void initState() {
    getWallet();
    super.initState();
  }

  void getWallet() async {
    HttpHandler _response = await model.getWallet(context);

    if (_response.result) {
      myWallet = _response.data;
      setState(() {
        internetConnexion = true;
      });
    } else {
      setState(() {
        internetConnexion = false;
      });
      showErrorHttpRequest(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: internetConnexion ? getView() : noConnexion());
  }

  Widget getView() {
    if (myWallet == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else if (myWallet.wallet == 'none') {
      return setWalletView();
    } else {
      return showWallet();
    }
  }

  Widget showWallet() {
    return Card(
      margin: EdgeInsets.all(20),
      child: Container(
        alignment: Alignment.center,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          QrImage(
            data: myWallet.wallet,
            version: QrVersions.auto,
            size: 200.0,
          ),
          Text(myWallet.wallet),
          RaisedButton(
            color: Colors.blueAccent,
            child: Text(
              'Copiar',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              Clipboard.setData(new ClipboardData(text: myWallet.wallet));
              var snackbar = new SnackBar(
                content: Text('Wallet copiado'),
                duration: Duration(seconds: 5),
                backgroundColor: Colors.blue,
              );
              Scaffold.of(context).showSnackBar(snackbar);
            },
          ),
        ]),
      ),
    );
  }

  Widget setWalletView() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Expanded(
          child: Card(
            margin: EdgeInsets.all(20),
            child: Container(
              alignment: Alignment.topCenter,
//            color: Colors.red,
              padding: EdgeInsets.all(16.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  slogan(),
                  generateWalletBtn(),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      'Click en el botón para generar dirección',
                      style: GoogleFonts.portLligatSans(
                          fontWeight: FontWeight.w100,
                          color: Colors.black,
                          fontSize: 17),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget slogan() {
    return Text(
      'Para fondear tu wallet de TeraBlok, debes generar una dirección en la cadena de bloques.',
      textAlign: TextAlign.center,
      style: GoogleFonts.portLligatSans(
          fontWeight: FontWeight.w100, color: Colors.black, fontSize: 17),
    );
  }

  Widget generateWalletBtn() {
    return Padding(
      padding: const EdgeInsets.only(top: 18.0),
      child: Container(
        margin: EdgeInsets.all(0.0),
        height: 100.0,
        width: 100.0,
        child: RaisedButton(
            padding: EdgeInsets.all(0.0),
            color: Colors.white,
            highlightElevation: 0.0,
            onPressed: () => setWallet(),
            highlightColor: Colors.transparent,
            shape: CircleBorder(
              side: BorderSide(color: Colors.grey, width: 2),
            ),
            child: Text(
              'Generar',
              style: TextStyle(fontSize: 20),
            )),
      ),
    );
  }

  Widget noConnexion() {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              'No se puede establecer una conexión con la red',
            ),
            leading: Icon(
              Icons.error,
              color: Colors.red,
            ),
          ),
          RaisedButton(
            child: Text('Reintentar'),
            onPressed: () {
              getWallet();
            },
          )
        ],
      ),
    );
  }

  void setWallet() async {
    // asignar Walet, despuess mostrarlo
    bool _result = await askTermAndConditions(context);
    if (_result) {
      HttpHandler _response = await model.setWallet(context);

      if (_response.result) {
        myWallet = _response.data;
        setState(() {
          internetConnexion = true;
        });
      } else {
        setState(() {
          internetConnexion = false;
        });
        showErrorHttpRequest(context);
      }

    }
  }
}
