import 'dart:async';

import 'package:TeraBlock/Modules/DashBoard/DashBoardView.dart';
import 'package:TeraBlock/Modules/GlobalPin/PinView.dart';
import 'package:TeraBlock/Others/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:TeraBlock/modules/Intro/IntroView.dart';
import 'package:TeraBlock/modules/Welcome/WelcomeView.dart';

class SplashView extends StatefulWidget {
  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  var splashDuration = Duration(seconds: 3);
  List<String> user;
  @override
  void initState() {
    initLoad();
    super.initState();
  }

  //load timer to exec loadService Event
  initLoad() {
    return new Timer(splashDuration, loadServices);
  }

  //mainEvent to get preferences.
  loadServices() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final intoId = prefs.getBool('intro') ?? false;
    user = prefs.getStringList('user') ?? null;
    final bool pin = prefs.getBool('pin') ?? false;

    if (!intoId) {
      Navigator.of(context).pop();
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => IntroView(),
        ),
      );
    } else {
      if (user == null) {
        Navigator.of(context).pop();
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => WelcomeView(),
          ),
        );
      } else {
        if (pin) if (await pinRequire()) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => DashBoard()),
              (Route<dynamic> route) => false);
        } else {
          SystemNavigator.pop();
        }
        else {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => DashBoard()),
              (Route<dynamic> route) => false);
        }
      }
    }
  }

  Future<bool> pinRequire() async {
    return await Navigator.push(
        context, MaterialPageRoute(builder: (context) => GlobalPin(type: 0)));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: HexColor('#0F2C45'),
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Center(
                  child: Container(
                      width: size.width * 0.2,
                      margin: EdgeInsets.all(20),
                      child: Image.asset('assets/Icons/splashicon.png')),
                ),
              ),
              Expanded(
                  flex: 1,
                  child: Column(
                    children: <Widget>[
                      CircularProgressIndicator(
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(Colors.blue),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
