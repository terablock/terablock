import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/Send/Modules/AddContact/AddContactView.dart';
import 'package:TeraBlock/Modules/Send/Modules/Transfer/TransferView.dart';
import 'package:TeraBlock/Modules/Send/SendModel.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:TeraBlock/navigation_bloc.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SendView extends StatefulWidget with NavigationStates {
  @override
  _SendViewState createState() => _SendViewState();
}

class _SendViewState extends State<SendView> {
  bool internetConexion = true;
  SendModel model = new SendModel();
  Contacts contacts;
  @override
  void initState() {
    getContacts();
    super.initState();
  }

  void addContact() async {
    bool result = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => AddContactView()));

    if (result!=null) {
      getContacts();
    }
  }

  void getContacts() async {
    HttpHandler _response = await model.getContacts(context);

    if (_response.result) {
      contacts = _response.data;

      setState(() {
        internetConexion = true;
      });
    } else {
      showErrorHttpRequest(context);
      setState(() {
        internetConexion = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: internetConexion
          ? FloatingActionButton.extended(
              label: Text('Agregar'),
              icon: Icon(
                Icons.add,
                size: 40,
              ),
              onPressed: () => addContact(),
            )
          : null,
      body: Container(
        child: Column(
          children: <Widget>[getView()],
        ),
      ),
    );
  }

  Widget getView() {
    if (!internetConexion) {
      return noConnexion();
    } else if (contacts == null) {
      return showWait();
    } else if (contacts.contacts.length == 0) {
      return notContactIntro();
    } else {
      return showContacts();
    }
  }

  Widget noConnexion() {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              'No se puede establecer una conexión con la red',
            ),
            leading: Icon(
              Icons.error,
              color: Colors.red,
            ),
          ),
          RaisedButton(
            child: Text('Reintentar'),
            onPressed: () {
              getContacts();
            },
          )
        ],
      ),
    );
  }

  Widget showContacts() {
    return Expanded(
      child: Card(
        margin: EdgeInsets.all(10),
        child: new ListView.builder(
            itemCount: contacts.contacts.length,
            itemBuilder: (BuildContext ctxt, int index) {
              return Column(
                children: [
                  new ListTile(
                    leading: getIcon(contacts.contacts[index].type),
                    title: Text(contacts.contacts[index].name),
                    trailing: Icon(Icons.keyboard_arrow_right),
                    subtitle: Text(
                      contacts.contacts[index].wallet,
                      style: TextStyle(color: Colors.green),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              TransferView(contact: contacts.contacts[index]),
                        ),
                      );
                    },
                  ),
                  Divider()
                ],
              );
            }),
      ),
    );
  }

  Widget notContactIntro() {
    return Card(
      margin: EdgeInsets.all(20),
      child: Container(
        margin: EdgeInsets.all(10),
        alignment: Alignment.center,
//        decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center, 
            children: [
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: 'Para retirar fondos debes dar de alta destinatarios Cryto, actualmente no tienes ninguna dirección crypto registrada',
              style: GoogleFonts.portLligatSans(
                fontWeight: FontWeight.w100,
                color: Colors.black,
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 18.0),
            child: Container(
              margin: EdgeInsets.all(0.0),
              height: 100.0,
              width: 100.0,
              child: Icon(
                Icons.perm_contact_calendar,
                color: Colors.blue,
                size: 100,
              ),
            ),
          )
        ]),
      ),
    );
  }
}
