import 'package:TeraBlock/Others/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:TeraBlock/Models/SignInInfo.dart';

class PasswordTextField extends StatefulWidget {
  final SignInInfo data;

  PasswordTextField(this.data);
  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  TextEditingController passwordController = new TextEditingController();
  bool _passwordRequire= false;

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: TextInputType.text,
      controller: passwordController,
      style: TextStyle(fontSize: 17, color: Colors.grey),
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(
            Icons.security,
            size: 30.0,
            color:  HexColor('#0F2C45'),
          ),
          errorText: _passwordRequire ? 'Contraseña es requerido.' : null,
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          hintText: 'Contraseña'),
      onChanged: (val) {
        setState(() {


        if(val.length>0){
          _passwordRequire = false;
          widget.data.password = val;
        }
        else{
          _passwordRequire = true;
          widget.data.password = null;
        }
        });
      },
    );
  }
}
