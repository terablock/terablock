import 'package:TeraBlock/Others/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/SignInInfo.dart';
import 'package:TeraBlock/Models/phoneInfo.dart';
import 'package:TeraBlock/modules/DashBoard/DashBoardView.dart';
import 'package:TeraBlock/modules/Sign/In/SignInModel.dart';
import 'package:TeraBlock/modules/Sign/In/Widgets/EmailTextField.dart';
import 'package:TeraBlock/modules/Sign/In/Widgets/PhoneTextField.dart';
import 'package:TeraBlock/others/Widgets.dart';
import 'package:TeraBlock/modules/Sign/In/Models/login.dart';
import 'Widgets/DialogCountry.dart';
import 'Widgets/PasswordTextField.dart';

class SignInView extends StatefulWidget {
  @override
  _SignInViewState createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  SignInModel model = new SignInModel();
  SignInInfo _signInInfo = new SignInInfo();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  void verifyForm() {
    if (_signInInfo.email == null) {
      _signInInfo.focusNodes.emailFocusNode.requestFocus();
      showRequired(1);
    } else if (_signInInfo.phone.numberPhone == null) {
      _signInInfo.focusNodes.phoneFocusNode.requestFocus();
      showRequired(2);
    } else if (_signInInfo.password == null) {
      showRequired(3);
    } else {
      login();
    }
  }

  void login() async {
    HttpHandler _response = await model.signIn(_signInInfo, context);
    if (_response.result) {

       Login data = new Login();
       data = _response.data;
      if (data.result == '1') {
        model.saveSession(data.iduUser, _signInInfo);

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => DashBoard()),
            (Route<dynamic> route) => false);
      } else {
        showLoginError(data);
      }
    } else {
      showErrorHttpRequest(context);
    }
  }

  void showRequired(int op) {
    switch (op) {
      case 1:
        {
          var snackbar = new SnackBar(
            content: Text('Email válido es requerido'),
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          );
          _scaffoldkey.currentState.showSnackBar(snackbar);
        }
        break;
      case 2:
        {
          var snackbar = new SnackBar(
            content: Text('Número celular es requerido'),
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          );
          _scaffoldkey.currentState.showSnackBar(snackbar);
        }
        break;
      case 3:
        {
          var snackbar = new SnackBar(
            content: Text('Contraseña es requerido'),
            duration: Duration(seconds: 2),
            backgroundColor: Colors.orange,
          );
          _scaffoldkey.currentState.showSnackBar(snackbar);
        }
        break;
      default:
        break;
    }
  }

  Future showLoginError(Login data) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
              title: Center(
                  child: Text(
                data.msj,
                textAlign: TextAlign.center,
              )),
              content: Icon(
                Icons.info,
                size: 70,
                color: Colors.blueAccent,
              ),
              contentPadding: EdgeInsets.all(10.0),
              actions: <Widget>[
                Center(
                    child:
                        Text('Verificar las credenciales e intenta de nuevo')),
                new FlatButton(
                    child: Text('Aceptar'),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldkey,
        appBar: AppBar(
          title: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Iniciar Sesión',
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.w300),
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          elevation: 0.0,
          backgroundColor: Colors.white,
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 45, right: 45),
                          child: Text(
                            'Escribe el correo, el  teléfono y contraseña con el que te registraste, para el acceso a tu cuenta.',
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.black54,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Divider(color: Colors.black45),
                        Padding(
                            padding: const EdgeInsets.only(left: 20.0),
                            child: EmailTextField(
                              data: _signInInfo,
                            )),
                        Divider(color: Colors.black45),
                        Row(
                          children: [
                            Expanded(
                              flex: 4,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20.0),
                                child: PhoneTextField(
                                  data: _signInInfo,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 10),
                                child: GestureDetector(
                                  child: _signInInfo.phone.countryImg,
                                onTap: () async{
                                  PhoneInfo phone = await dialogCountry(context);
                                  setState(() {
                                    _signInInfo.phone.dialCode = phone.dialCode;
                                    _signInInfo.phone.countryImg = phone.countryImg;
                                    _signInInfo.phone.countryName = phone.countryName;
                                  });

                                },)
                                ,
                              ),
                            )
                          ],
                        ),
                        Divider(color: Colors.black45),
                        Padding(
                            padding: const EdgeInsets.only(left: 20.0),
                            child: PasswordTextField(_signInInfo)),
                        Divider(
                          color: Colors.black45,
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Text(
                '¿Necesitas Ayuda?',
                style: TextStyle(fontSize: 17, color: Colors.black),
              ),
              RichText(
                text: TextSpan(
                    style: TextStyle(fontSize: 17, color: Colors.grey),
                    text: 'Escribenos a ',
                    children: [
                      TextSpan(
                        text: 'developer@highlevercap.com',
                        style: TextStyle(
                            fontSize: 17, color: Colors.deepPurpleAccent),
                      ),
                    ]),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.all(0),
                width: MediaQuery.of(context).size.width,
                height: 50,
                child: RaisedButton(
                  color:  HexColor('#0F2C45'),
                  onPressed: () {
                    verifyForm();
//                    Navigator.push(
//                      context,
//                      MaterialPageRoute(builder: (context) => DashBoard()),
//                    );
                  },
                  child: Text(
                    'Iniciar sesión',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        fontSize: 17),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  @override
  void dispose() {
    _signInInfo.dispose();
    super.dispose();
  }
}
