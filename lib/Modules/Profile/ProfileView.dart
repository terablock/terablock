import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/Profile/EditProfileView.dart';
import 'package:TeraBlock/Modules/Profile/ProfileModel.dart';
import 'package:TeraBlock/Others/HexColor.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  bool internetConnexion = true;
  ProfileInfo _profileInfo;
  ProfileModel model = new ProfileModel();
  String phone;

  @override
  void initState() {
    getProfile();
    super.initState();
  }

  getProfile() async {
   phone = await User().getPhone();
    HttpHandler response = await model.getProfile(context);
    if (response.result) {
      setState(() {
        internetConnexion = true;
        _profileInfo = response.data;
      });

    } else {
      setState(() {
        internetConnexion = false;
      });
      showErrorHttpRequest(context);
    }
  }

  Widget getView() {
    if (!internetConnexion) {
      return noConnexion();
    } else if (_profileInfo != null) {
      return showProfile();
    } else {
      return Container(child: Column(children: [showWait()]));
    }
  }

  Widget noConnexion() {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              'No se puede establecer una conexión con la red',
            ),
            leading: Icon(
              Icons.error,
              color: Colors.red,
            ),
          ),
          RaisedButton(
            child: Text('Reintentar'),
            onPressed: () {
              getProfile();
            },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: internetConnexion
          ? FloatingActionButton.extended(
              label: Text('Editar'),
              icon: Icon(
                Icons.edit,
                size: 40,
              ),
              onPressed: () => editProfile(),
            )
          : null,
      appBar: AppBar(
        title: Text(
          'Perfil de usuario',
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      body: getView(),
    );
  }

  Widget showProfile() {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    child: Image.asset(
                  'assets/Icons/terablock.png',
                  width: MediaQuery.of(context).size.width / 2,
                )),
                Text(
                  'perfiles',
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                      color: HexColor('#0F2C45')),
                )
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              margin: EdgeInsets.all(10),
//                  color: Colors.blue,
              child: Card(
                child: ListView(
                  children: [
                    Column(
                      children: <Widget>[
                        TextField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person),
                              hintText: '${_profileInfo.name} ${_profileInfo.fatherSurname} ${_profileInfo.motherSurname}'),
                          readOnly: true,
                        ),
                        TextField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.mail),
                              hintText: _profileInfo.mail),
                          readOnly: true,
                        ),
                        TextField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.phone),
                              hintText: phone),
                          readOnly: true,
                        ),
                        TextField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.cake),
                              hintText: DateFormat.yMMMMd('es').format(DateTime.parse(_profileInfo.birthDate))),
                          readOnly: true,
                        ),
                        TextField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.location_on),
                              hintText: ' Pais: ${_profileInfo.country}'),
                          readOnly: true,
                        ),
                        TextField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.location_on),
                              hintText: ' Estado: ${_profileInfo.state}'),
                          readOnly: true,
                        ),
                        TextField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.location_on),
                              hintText: ' Ciudad: ${_profileInfo.city}'),
                          readOnly: true,
                        ),
                        TextField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.markunread_mailbox),
                              hintText: 'Código Postal: ${_profileInfo.zipCode}'),
                          readOnly: true,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  editProfile() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => EditProfileView(
            profileInfo: _profileInfo,
          )),
    );
  }
}
