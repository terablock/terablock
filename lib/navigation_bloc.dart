import 'package:TeraBlock/Modules/History/HistoryView.dart';
import 'package:TeraBlock/Modules/Send/SendView.dart';
import 'package:TeraBlock/Modules/receive/ReceiveView.dart';
import 'package:bloc/bloc.dart';
import 'Modules/Home/HomeView.dart';



enum NavigationEvents {
  HomePageClickedEvent,
  MyHistory,
  MySendEvent,
  MyReceiveEvent,
}

abstract class NavigationStates {}

class NavigationBloc extends Bloc<NavigationEvents, NavigationStates> {
  @override
  NavigationStates get initialState => HomePage();

  @override
  Stream<NavigationStates> mapEventToState(NavigationEvents event) async* {
    switch (event) {
      case NavigationEvents.HomePageClickedEvent:
        yield HomePage();
        break;
      case NavigationEvents.MyHistory:
        yield HistoryView();
        break;
      case NavigationEvents.MySendEvent:
        yield SendView();
        break;
      case NavigationEvents.MyReceiveEvent:
        yield ReceiveView();
        break;
    }
  }
}