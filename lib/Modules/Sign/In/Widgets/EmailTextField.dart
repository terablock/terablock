import 'package:TeraBlock/Others/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:TeraBlock/Models/SignInInfo.dart';

class EmailTextField extends StatefulWidget {
  final SignInInfo data;

  EmailTextField({this.data});

  @override
  _EmailTextFieldState createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {
  bool emailError = false;
  @override
  Widget build(BuildContext context) {
    return TextField(
      focusNode: widget.data.focusNodes.emailFocusNode,
      controller: widget.data.controllers.emailController,
      keyboardType: TextInputType.emailAddress,
      style: TextStyle(fontSize: 17, color: Colors.grey),
      decoration: InputDecoration(
          icon: Icon(
            Icons.email,
            size: 30.0,
            color:  HexColor('#0F2C45'),
          ),
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          errorText: emailError ? 'Email invalido' : null,
          hintText: 'Correo Electronico'),
      onChanged: (val) {
        bool emailValid = RegExp(r"^([\w\.\-_]+)?\w+@[\w-_]+(\.\w+){1,}$")
            .hasMatch(val.trim());

        setState(() {
          emailError = !emailValid;
        });

        widget.data.email = emailValid ? val.trim() : null;
      },
    );
  }
}
