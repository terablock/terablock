import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/Sign/Up/SignUpModel.dart';
import 'package:TeraBlock/Others/HexColor.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RegisterUser extends StatefulWidget {
  final SignUpInfo signUpInfo;

  RegisterUser({this.signUpInfo});

  @override
  _RegisterUserState createState() => _RegisterUserState();
}

class _RegisterUserState extends State<RegisterUser> {
  TextEditingController emailController;
  TextEditingController passwordController;
  TextEditingController verifyPasswordController;
  FocusNode _focusNodeEmail;
  FocusNode _focusNodePassword;
  FocusNode _focusNodeVerifyPassword;
  SignUpModule model;

  String email;
  bool emailError = false;
  bool _showPassword = false;
  bool _inputPassword = true;
  bool _showPasswordVerify = false;
  bool _inputPasswordVerify = true;

  @override
  void initState() {
//
    model = new SignUpModule();
//   init Controllers
    emailController = new TextEditingController();
    passwordController = new TextEditingController();
    verifyPasswordController = new TextEditingController();
//    init FocusNode
    _focusNodeEmail = new FocusNode();
    _focusNodePassword = new FocusNode();
    _focusNodeVerifyPassword = new FocusNode();
    super.initState();
  }

  @override
  void dispose() {
//    Controllers dispose
    emailController.dispose();
    passwordController.dispose();
    verifyPasswordController.dispose();
//    Focus Node despose
    _focusNodePassword.dispose();
    _focusNodeVerifyPassword.dispose();
    _focusNodeEmail.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              'Registro de usuario',
              style: TextStyle(color: Colors.black),
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          elevation: 0.0,
          backgroundColor: Colors.white,
        ),
        body: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                height: 20,
                width: MediaQuery.of(context).size.width,
                color: HexColor('#0F2C45'),
                child: Center(
                    child:
                        Text('3 de 3', style: TextStyle(color: Colors.white))),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 45, right: 45, top: 20),
              child: Text(
                'Ya casi terminas tu registro, favor de ingresar los siguientes datos solicitados.',
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.black54,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Divider(color: Colors.black45),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: ListView(
//                height: MediaQuery.of(context).size.width/2,
                children: [
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            TextField(
                              controller: emailController,
                              focusNode: _focusNodeEmail,
                              keyboardType: TextInputType.emailAddress,
                              style:
                                  TextStyle(fontSize: 17, color: Colors.grey),
                              decoration: InputDecoration(
                                  icon: Icon(
                                    Icons.mail,
                                    color: HexColor('#0F2C45'),
                                  ),
                                  labelText: 'Correo electronico',
                                  errorText: emailError
                                      ? 'Correo electronico invalido'
                                      : null,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0)))),
                              onChanged: (val) {
                                bool emailValid = RegExp(
                                        r"^([\w\.\-_]+)?\w+@[\w-_]+(\.\w+){1,}$")
                                    .hasMatch(val.trim());

                                setState(() {
                                  emailError = !emailValid;
                                });
                                email = emailValid ? val : null;
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextField(
                                controller: passwordController,
                                focusNode: _focusNodePassword,
                                keyboardType: TextInputType.emailAddress,
                                style: Theme.of(context).textTheme.bodyText1,
                                obscureText: !this._showPassword,
                                maxLength: 15,
                                decoration: InputDecoration(
//                                    prefix: Icon(Icons.security),
                                    icon: Icon(
                                      Icons.security,
                                      color: HexColor('#0F2C45'),
                                    ),
                                    labelText: 'Contraseña',
                                    suffixIcon: IconButton(
                                      icon: Icon(Icons.remove_red_eye),
                                      color: this._showPassword
                                          ? HexColor('#0F2C45')
                                          : Colors.grey,
                                      onPressed: () {
                                        setState(() {
                                          _showPassword = !_showPassword;
                                        });
                                      },
                                    ),
                                    errorText: _inputPassword
                                        ? null
                                        : 'La contrasena debe tener al menos 7 caracteres',
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)))),
                                onChanged: (val) {
                                  verifyPasswordController.text = '';
                                  _inputPasswordVerify = true;
                                  if (val.length < 7) {
                                    setState(() {
                                      _inputPassword = false;
                                    });
                                  } else {
                                    setState(() {
                                      _inputPassword = true;
                                    });
                                  }
                                }),
                            TextField(
                                controller: verifyPasswordController,
                                focusNode: _focusNodeVerifyPassword,
                                keyboardType: TextInputType.text,
                                style: Theme.of(context).textTheme.bodyText1,
                                obscureText: !this._showPasswordVerify,
                                maxLength: 15,
                                decoration: InputDecoration(
                                    icon: Icon(Icons.security,
                                        color: HexColor('#0F2C45')),
                                    labelText: 'Confirmar Contraseña',
                                    suffixIcon: IconButton(
                                      icon: Icon(Icons.remove_red_eye),
                                      color: this._showPasswordVerify
                                          ? HexColor('#0F2C45')
                                          : Colors.grey,
                                      onPressed: () {
                                        setState(() {
                                          _showPasswordVerify =
                                              !_showPasswordVerify;
                                        });
                                      },
                                    ),
                                    errorText: _inputPasswordVerify
                                        ? null
                                        : 'Las contrasenas no coinciden ',
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)))),
                                onChanged: (val) {
                                  if (val == passwordController.text) {
                                    setState(() {
                                      _inputPasswordVerify = true;
                                    });
                                  } else {
                                    setState(() {
                                      _inputPasswordVerify = false;
                                    });
                                  }
                                }),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: 'Al crear una cuenta, usted acepta los',
                              style: GoogleFonts.portLligatSans(
                                textStyle:
                                    Theme.of(context).textTheme.bodyText1,
                                fontWeight: FontWeight.w100,
                                color: Colors.black,
                              ),
                              children: [
                                TextSpan(
                                  text: ' Terminos del servicio',
                                  style: TextStyle(
                                    color: Colors.blue,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                                TextSpan(
                                  text: ' & ',
                                ),
                                TextSpan(
                                  text: 'Politica de privacidad ',
                                  style: TextStyle(
                                      color: Colors.blue,
                                      decoration: TextDecoration.underline),
                                ),
                                TextSpan(
                                  text: ' de TeraBlock',
                                ),
                              ]),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Center(
                          child: Align(
                            alignment: Alignment.center,
                            child: ButtonTheme(
                              minWidth: MediaQuery.of(context).size.width,
                              height: 50,
                              child: RaisedButton(
                                color: HexColor('#0F2C45'),
                                shape: ContinuousRectangleBorder(
                                    borderRadius: BorderRadius.circular(4.0)),
                                child: Text(
                                  "Registrar",
                                  style: TextStyle(color: Colors.white),
                                ),
                                onPressed: () => verifyForm(),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void saveContact() async {
    bool terCon = await askTermAndConditions(context);
    if (terCon) {
      bool result = await askVerifyMail(context, emailController.text);
      if (!result) {
        _focusNodeEmail.requestFocus();
      } else {
        SignUpInfo info = widget.signUpInfo;
        info.mail = emailController.text;
        info.password = passwordController.text;
        HttpHandler resultNewUser = await model.registerUser(context, info);
        if (resultNewUser.result) {
          GlobalResult _result = resultNewUser.data;
          if (_result.result == '1') {
            showSuccessCreateAccount(context, emailController.text);
          } else {
            showEmailUnavailable(context, emailController, _focusNodeEmail);
          }
        } else {
          showErrorHttpRequest(context);
        }
      }
    }
  }

  void verifyForm() {
    if (emailError || emailController.text.length == 0) {
      showEmailRequire(context, emailController, _focusNodeEmail);
    } else if (passwordController.text.length < 7) {
      showPasswordRequire(context, passwordController, _focusNodePassword);
    } else if (passwordController.text != verifyPasswordController.text) {
      showPasswordVerify(
          context, verifyPasswordController, _focusNodeVerifyPassword);
    } else {
      saveContact();
    }
  }
}
