import 'package:firebase_database/firebase_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class User {
  Future<String> getUserID() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    List<String> user = prefs.getStringList('user');
    return user[0];
  }

  Future<String> getPhone() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    List<String> user = prefs.getStringList('user');
    return user[1];
  }
}

class UserBalance {
  String balance;
  String accountStatus;
  String profileStatus;
  final List<Transfer> transfers;

  UserBalance(
      {this.balance, this.transfers, this.accountStatus, this.profileStatus});

  factory UserBalance.fromJson(Map<String, dynamic> json) {
    List<Transfer> history = new List<Transfer>();

    List<dynamic> jsonTransactonsJson = json['transactions'];
    if (jsonTransactonsJson != null) {
      history = jsonTransactonsJson.map((i) => Transfer.fromJson(i)).toList();
    }
    return UserBalance(
        balance: json['balance'],
        profileStatus: json['profile_status'],
        accountStatus: json['status'],
        transfers: history);
  }
}

class Transfers {
  final List<Transfer> transfers;
  Transfers({this.transfers});

  factory Transfers.fromJson(List<dynamic> json) {
    List<Transfer> transfer = new List<Transfer>();
    if (json != null) {
      transfer = json.map((i) => Transfer.fromJson(i)).toList();
    }

    return new Transfers(transfers: transfer);
  }
}

class Transfer {
  String idu;
  String note;
  String type;
  String typeName;
  String amount;
  String createDate;
  String transaction;
  String descTransaction;
  String to;
  String walletId;

  Transfer(
      {this.idu,
      this.note,
      this.type,
      this.typeName,
      this.amount,
      this.createDate,
      this.transaction,
      this.descTransaction,
      this.to,
      this.walletId});

  factory Transfer.fromJson(Map<String, dynamic> json) {
    return Transfer(
        idu: json['idu'],
        note: json['note'],
        type: json['type'],
        typeName: json['name_type'],
        amount: json['amount'],
        createDate: json['create_date'],
        transaction: json['transaction'],
        descTransaction: json['desc_transaction'],
        to: json['toWallet'],
        walletId: json['to_idu_wallet']);
  }
}

class Wallet {
  String wallet;

  Wallet({this.wallet});

  factory Wallet.fromJson(Map<String, dynamic> json) {
    String _wallet;
    if (json == null) {
      _wallet = 'none';
    } else {
      _wallet = json['wallet'];
    }
    return Wallet(wallet: _wallet);
  }
}

class Contacts {
  List<Contact> contacts;

  Contacts({this.contacts});

  factory Contacts.fromJson(List<dynamic> json) {
    List<Contact> contacts = new List<Contact>();
    if (json != null) {
      contacts = json.map((i) => Contact.fromJson(i)).toList();
    }

    return new Contacts(contacts: contacts);
  }
}

class Contact {
  String idu;
  String wallet;
  String name;
  int type;
  String typeName;
  String amount;
  String note;

  Contact(
      {this.wallet,
      this.type,
      this.idu,
      this.typeName,
      this.name,
      this.amount,
      this.note});

  factory Contact.fromJson(Map<String, dynamic> json) {
    return Contact(
        idu: json['idu'],
        wallet: json['wallet'],
        type: json['type'],
        typeName: json['typeName'],
        name: json['name']);
  }
}

class CryptoTypes {
  final List<CryptoType> cryptoType;

  CryptoTypes({this.cryptoType});

  factory CryptoTypes.fromJson(List<dynamic> json) {
    List<CryptoType> cryptoTypeList = new List<CryptoType>();
    cryptoTypeList = json.map((i) => CryptoType.fromJson(i)).toList();

    return new CryptoTypes(cryptoType: cryptoTypeList);
  }
}

class CryptoType {
  String idu;
  String name;
  String type;

  CryptoType({this.idu, this.name, this.type});

  factory CryptoType.fromJson(Map<String, dynamic> json) {
    return CryptoType(idu: json['idu'], name: json['name']);
  }
}

class GlobalResult {
  String result;
  String msj;

  GlobalResult({this.result, this.msj});

  factory GlobalResult.fromJson(Map<String, dynamic> json) {
    return GlobalResult(result: json['result'], msj: json['msj']);
  }
}

class TransferInfo {
  User user;
  Contact contact;

  TransferInfo({this.user, this.contact});
}

class TransferResult {
  String result;
  String iduTransfer;
  String msj;

  TransferResult({this.result, this.iduTransfer, this.msj});

  factory TransferResult.fromJson(Map<String, dynamic> json) {
    return TransferResult(
        result: json['result'],
        iduTransfer: json['idu_transaction'],
        msj: json['msj']);
  }
}

class SignUpInfo {
  String sponsor;
  int typeSponsor;
  String phoneNumber;
  String mail;
  String password;

  SignUpInfo(
      {this.sponsor,
      this.typeSponsor,
      this.phoneNumber,
      this.mail,
      this.password});
}

class NotificationGlobal {
  String _idu;
  String _title;
  String _description;
  String _picture;

  NotificationGlobal(this._idu, this._title, this._description, this._picture);

  NotificationGlobal.map(dynamic obj) {
    this._idu = obj['idu'];
    this._title = obj['Title'];
    this._description = obj['description'];
    this._picture = obj['description'];
  }

  String get id => _idu;

  String get title => _title;

  String get description => _description;

  String get picture => _picture;

  NotificationGlobal.fromSnapshot(DataSnapshot snapshot) {
    _idu = snapshot.key;
    _title = snapshot.value['title'];
    _description = snapshot.value['description'];
    _picture = snapshot.value['picture'];
  }
}

class ProfileInfo {
  String name;
  String fatherSurname;
  String motherSurname;
  String birthDate;
  String country;
  String state;
  String city;
  String zipCode;
  String mail;

  ProfileInfo(
      {this.name,
      this.fatherSurname,
      this.motherSurname,
      this.birthDate,
      this.country,
      this.state,
      this.city,
      this.zipCode,
      this.mail});

  factory ProfileInfo.fromJson(Map<String, dynamic> json) {
    return ProfileInfo(
        name: json['name'],
        fatherSurname: json['fatherName'],
        motherSurname: json['motherName'],
        birthDate: json['birthday'],
        country: json['country'],
        state: json['state'],
        city: json['city'],
        zipCode: json['zipCode'],
    mail: json['mail']);
  }
}
