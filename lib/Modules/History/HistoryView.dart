import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/History/HistoryModel.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:TeraBlock/navigation_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:intl/intl.dart'; // for date format

class HistoryView extends StatefulWidget with NavigationStates {
  @override
  _HistoryViewState createState() => _HistoryViewState();
}

class _HistoryViewState extends State<HistoryView> {
  DateTime _time = DateTime.now();
  Transfers transfers = new Transfers();
  HistoryModel model = new HistoryModel();
  bool internetConexion = true;
  @override
  void initState() {
    getHistory();
    super.initState();
  }

  void _datePickerDialog(BuildContext context) {

    DatePicker.showDatePicker(
      context,
      minDateTime: DateTime(1940, 01, 1),
      maxDateTime: DateTime.now(),
      initialDateTime: _time,
      dateFormat: 'MMMM-yyyy',
      locale: DateTimePickerLocale.es,
      pickerTheme: DateTimePickerTheme(
//        showTitle: _showTitle,
          ),
      pickerMode: DateTimePickerMode.date, // show TimePicker
      onCancel: () {
        debugPrint('onCancel');
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _time = dateTime;
          getHistory();
        });
      },
    );
  }

  Widget noConnexion() {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              'No se puede establecer una conexión con la red',
            ),
            leading: Icon(
              Icons.error,
              color: Colors.red,
            ),
          ),
          RaisedButton(
            child: Text('Reintentar'),
            onPressed: () {
              getHistory();
            },
          )
        ],
      ),
    );
  }

  void getHistory() async {
    HttpHandler _response = await model.getTransfers(context, _time);

    if (_response.result) {
      setState(() {
        transfers = _response.data;
        internetConexion = true;
      });
    } else {
      setState(() {
        internetConexion = false;
      });
      showErrorHttpRequest(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: internetConexion
            ? Container(
               
                child: Column(
                  children: <Widget>[
                    showHistory(context, transfers.transfers),
                    Container(
                      height: 90,
                      width: MediaQuery.of(context).size.width,
                      child: Container(

                        child: Row(
//                            mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(

                              flex: 1,
                              child: Container(
                                child: FlatButton.icon(
                                    onPressed: () {
                                      setState(() {
                                        _time = DateTime(_time.year,
                                            _time.month - 1, _time.day);
                                        getHistory();
                                      });
                                    },
                                    icon: Icon(
                                      Icons.keyboard_arrow_left,
                                      size: 30,
                                    ),
                                    label: Text('')),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                _datePickerDialog(context);
                              },
                              child: Container(
                                height: 40,
                              width: (MediaQuery.of(context).size.width/5)*3,
//                            color: Colors.blue,
                                decoration: BoxDecoration(
                                  color: Colors.white12,
                                  border: Border.all(
                                    color: Colors.black12,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    RichText(
                                      text: TextSpan(
                                        style: DefaultTextStyle.of(context)
                                            .style,
                                        children: <TextSpan>[
                                          TextSpan(
                                              text: DateFormat.yMMMM('es')
                                                  .format(_time),
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight:
                                                      FontWeight.bold)),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                child: FlatButton.icon(
                                    onPressed: () {
                                      if (_time.year < DateTime.now().year) {
                                        setState(() {
                                          _time = DateTime(_time.year,
                                              _time.month + 1, _time.day);
                                          getHistory();

                                        });
                                      } else {
                                        if (_time.month <
                                            DateTime.now().month) {
                                          setState(() {
                                            _time = DateTime(_time.year,
                                                _time.month + 1, _time.day);
                                            getHistory();

                                          });
                                        }
                                      }
                                    },
                                    icon: Icon(
                                      Icons.keyboard_arrow_right,
                                      size: 30,
                                    ),
                                    label: Text('')),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ))
            : noConnexion());
  }
}
