import 'package:TeraBlock/Modules/Sign/Up/SignUpView.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:TeraBlock/modules/Sign/In/SignInView.dart';
import 'package:TeraBlock/others/HexColor.dart';

class WelcomeView extends StatefulWidget {
  @override
  _WelcomeViewState createState() => _WelcomeViewState();
}

class _WelcomeViewState extends State<WelcomeView> {

  Widget _slogan() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text:
              'La mejor manera de enviar, recivir,almacenar y comprar monedas digitales',
          style: GoogleFonts.portLligatSans(
            textStyle: Theme.of(context).textTheme.bodyText1,
            fontSize: 15,
            fontWeight: FontWeight.w200,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Image.asset('assets/Icons/terablock.png'),
                    ],
                  ),
                )),
            Expanded(
                flex: 1,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [ _slogan()])),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                children: <Widget>[
                  Center(
                    child: Align(
                      alignment: Alignment.center,
                      child: ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width,
                        height: 60,
                        child: RaisedButton(
                          color: Colors.white,
                          shape: ContinuousRectangleBorder(
                              borderRadius: BorderRadius.circular(4.0)),
                          child: Text(
                            "Crear Cuenta",
                            style: TextStyle(
                                color: HexColor('#0F2C45'), fontSize: 17),
                          ),
                          onPressed: () async {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignUpView()),
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width,
                      height: 60,
                      child: FlatButton(
                        color: HexColor('#0F2C45'),
                        child: Text(
                          "Iniciar Sesion",
                          style: TextStyle(color: Colors.white, fontSize: 17),
                        ),
                        onPressed: () async {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignInView()),
                          );
                        },
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}