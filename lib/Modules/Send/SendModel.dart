import 'dart:convert';

import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';

ProgressDialog pr;

class SendModel {
  Future<HttpHandler> getContacts(BuildContext context) async {
    HttpHandler _response = new HttpHandler();
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );
    String userID = await User().getUserID();

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    try {
      final response = await http
          .get('https://terablock.app/terablock/api/contacts/$userID')
          .timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = Contacts.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';
    }
    await pr.hide();
    return _response;
  }

  Future<HttpHandler> getCryptos(BuildContext context) async {
    HttpHandler _response = new HttpHandler();
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    try {
      final response = await http
          .get('https://terablock.app/terablock/api/cryptos')
          .timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = CryptoTypes.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';
    }
    await pr.hide();
    return _response;
  }

  Future<HttpHandler> addContact(BuildContext context, Contact contact) async {
    HttpHandler _response = new HttpHandler();
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );
    String userID = await User().getUserID();

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    try {
      final response = await http
          .post('https://terablock.app/terablock/api/contacts',
              body: jsonEncode(<String, String>{
                'userID': userID,
                'type': contact.type.toString(),
                'wallet': contact.wallet,
                'name': contact.name,
              }))
          .timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = GlobalResult.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';
    }
    await pr.hide();
    return _response;
  }

  Future<HttpHandler> executeTransfer(
      BuildContext context, Contact contact, String iduCode) async {
    HttpHandler _response = new HttpHandler();
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );

    pr.style(
      message: 'Espere',
    );

    await pr.show();

    String userID = await User().getUserID();

    try {
      final response = await http
          .post('https://terablock.app/terablock/api/transfers',
              body: jsonEncode(<String, String>{
                'amount': contact.amount,
                'note': contact.note,
                'type': contact.type.toString(),
                'fromwallet': userID,
                'towallet': contact.wallet,
                'iduCode': iduCode
              }))
          .timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = TransferResult.fromJson(data);
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';
    }
    await pr.hide();

    return _response;
  }

  Future<HttpHandler> sendCode(BuildContext context) async {
    HttpHandler _response = new HttpHandler();
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );

    pr.style(
      message: 'Espere',
    );

    await pr.show();
    String phone = await User().getPhone();

    try {
      final response = await http
          .post('https://terablock.app/terablock/api//verify/phone',
              body: jsonEncode(<String, String>{'phone': phone}))
          .timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body)['data']['response'];
        _response.result = true;
        _response.data = data;
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';
    }
    await pr.hide();

    return _response;
  }

  Future<HttpHandler> verifyCode(
      BuildContext context, String code, String idu) async {
    HttpHandler _response = new HttpHandler();
    pr = ProgressDialog(
      context,
      isDismissible: false,
    );

    pr.style(
      message: 'Espere',
    );

    await pr.show();

    try {
      final response = await http
          .post('https://terablock.app/terablock/api//verify/code',
              body: jsonEncode(<String, String>{'idu': idu, 'code': code}))
          .timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        var data = GlobalResult.fromJson(
            jsonDecode(response.body)['data']['response']);
        _response.result = true;
        _response.data = data;
      } else {
        _response.result = false;
        _response.errorCode = response.statusCode.toString();
        _response.mjs = response.body;
      }
    } catch (ex) {
      _response.result = false;
      _response.errorCode = ex.toString();
      _response.mjs = 'Error en try catch';
    }
    await pr.hide();

    return _response;
  }
}
