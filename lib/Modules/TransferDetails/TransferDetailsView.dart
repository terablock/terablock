import 'package:TeraBlock/Models/User.dart';
import 'package:flutter/material.dart';

class TransferDetails extends StatelessWidget {
  TransferDetails({this.transfer});
  final Transfer transfer;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Detalles de transacción',
            style: TextStyle(color: Colors.black),
          ),
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          elevation: 0.0,
          backgroundColor: Colors.white,
        ),
        body: ListView(
//      padding: EdgeInsets.all(10.0),
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Card(
//          color: Colors.white70,
                child: Center(
                  child: Column(children: <Widget>[
                    Text(transfer.descTransaction,
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                            fontSize: 35)),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(),
                    Icon(
                      Icons.check_circle_outline,
                      size: 100,
                      color: Colors.green,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text('Transacción procesada con éxito',
                        style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text('Hash',
                        style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.normal,
                            fontSize: 20)),
                    Center(
                      child: Text(
                          "0x${transfer.idu.replaceAll('-', '')}${transfer.idu.replaceAll('-', '')}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                            fontSize: 15),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Divider(),
                    Text('Datos de Transacción',
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                            fontSize: 20)),
                    SizedBox(
                      height: 10,
                    ),
                    if (transfer.transaction == '1')
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Beneficiario:',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 15)),
                          Text(
                            '...' +
                                transfer.to.substring(transfer.to.length - 10,
                                    transfer.to.length),
                            style: TextStyle(color: Colors.grey, fontSize: 15),
                          )
                        ],
                      ),
                    Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Importe:',
                            style: TextStyle(color: Colors.grey, fontSize: 15)),
                        Text('\$ ' + transfer.amount,
                            style: TextStyle(color: Colors.blue, fontSize: 15)),
                      ],
                    ),
                    Divider(),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text('Concepto:',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 15)),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20.0, right: 20, top: 10),
                          child: Card(
                            child: Text(transfer.note,textAlign: TextAlign.center,
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 15)),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(30.0),
                      child: Center(
                        child: Align(
                          alignment: Alignment.center,
                          child: ButtonTheme(
                            minWidth: MediaQuery.of(context).size.width,
                            height: 50,
                            child: RaisedButton(
                              color: Colors.blue,
                              shape: ContinuousRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Text(
                                "Aceptar",
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                Navigator.pop(context, true);
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ),
          ],
        ));
  }
}
