import 'package:TeraBlock/Models/HttpHandler.dart';
import 'package:TeraBlock/Models/User.dart';
import 'package:TeraBlock/Modules/Profile/ProfileModel.dart';
import 'package:TeraBlock/Others/HexColor.dart';
import 'package:TeraBlock/Others/Widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:intl/intl.dart';

class EditProfileView extends StatefulWidget {
  final ProfileInfo profileInfo;

  EditProfileView({this.profileInfo});
  @override
  _EditProfileViewState createState() => _EditProfileViewState();
}

class _EditProfileViewState extends State<EditProfileView> {
  ProfileInfo _profileInfo;
  TextEditingController ctlName;
  TextEditingController ctlFatherName;
  TextEditingController ctlMotherName;
  TextEditingController ctlBirthDate;
  TextEditingController ctlCountry;
  TextEditingController ctlState;
  TextEditingController ctlCity;
  TextEditingController ctlZipCode;

  FocusNode focusName;
  FocusNode focusFatherName;
  FocusNode focusMotherName;
  FocusNode focusBirthDate;
  FocusNode focusCountry;
  FocusNode focusState;
  FocusNode focusCity;
  FocusNode focusZipCode;

  ProfileModel model = new ProfileModel();
  @override
  void initState() {
    if (widget.profileInfo == null) {
      _profileInfo = new ProfileInfo();
      initCtl();
      initFocus();
    }else{
      initCtl();
      initFocus();
      _profileInfo = widget.profileInfo;
      loadValuesFromProfile();

    }

    super.initState();

  }

  void initFocus() {
    focusName = new FocusNode();
    focusFatherName = new FocusNode();
    focusMotherName = new FocusNode();
    focusBirthDate = new FocusNode();
    focusCountry = new FocusNode();
    focusState = new FocusNode();
    focusCity = new FocusNode();
    focusZipCode = new FocusNode();
  }

  void initCtl() {
    ctlName = new TextEditingController();
    ctlFatherName = new TextEditingController();
    ctlMotherName = new TextEditingController();
    ctlBirthDate = new TextEditingController();
    ctlCountry = new TextEditingController();
    ctlState = new TextEditingController();
    ctlCity = new TextEditingController();
    ctlZipCode = new TextEditingController();

  }

  loadValuesFromProfile() {
    ctlName.text = '${widget.profileInfo.name}';
    ctlFatherName.text = widget.profileInfo.fatherSurname;
    ctlMotherName.text = widget.profileInfo.motherSurname;
    ctlBirthDate.text = DateFormat.yMMMMd('es')
        .format(DateTime.parse(widget.profileInfo.birthDate));
    ctlCountry.text = widget.profileInfo.country;
    ctlState.text = widget.profileInfo.state;
    ctlCity.text = widget.profileInfo.city;
    ctlZipCode.text = widget.profileInfo.zipCode;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Perfil de usuario'),
        ),
        body: editProfile());
  }

  Widget editProfile() {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    child: Image.asset(
                  'assets/Icons/terablock.png',
                  width: MediaQuery.of(context).size.width / 2,
                )),
                Text(
                  'perfiles',
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                      color: HexColor('#0F2C45')),
                )
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              margin: EdgeInsets.all(10),
              child: Card(
                child: ListView(
                  children: [
                    Column(
                      children: <Widget>[
                        TextField(
                          controller: ctlName,
                          focusNode: focusName,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person),
                              hintText: 'Nombres'),
//
//                      readOnly: true,
                        ),
                        TextField(
                            controller: ctlFatherName,
                            focusNode: focusFatherName,
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person),
                                hintText: 'Apellido Paterno')
//
//                      readOnly: true,
                            ),
                        TextField(
                            controller: ctlMotherName,
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person),
                                hintText: 'Apellido Materno')
//
//                      readOnly: true,
                            ),
                        TextField(
                          readOnly: true,
                          controller: ctlBirthDate,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.cake),
                              hintText: 'Fecha de nacimiento'),
//                              DateFormat.yMMMMd('es').format(DateTime.parse(_profileInfo.birthDate))),
//                      readOnly: true,
                          onTap: () {
                            _datePickerDialog(context);
                          },
                        ),
                        TextField(
                            controller: ctlCountry,
                            focusNode: focusCountry,
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.location_on),
                                hintText: 'Pais')
//                              ' Pais: ${_profileInfo.country}'),
//                      readOnly: true,
                            ),
                        TextField(
                            controller: ctlState,
                            focusNode: focusState,
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.location_on),
                                hintText: ' Estado ')
//                              ${_profileInfo.state}'),
//                      readOnly: true,
                            ),
                        TextField(
                          controller: ctlCity,
                          focusNode: focusCity,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.location_on),
                              hintText: ' Ciudad'),
//                            ${_profileInfo.city}'),
//                      readOnly: true,
                        ),
                        TextField(
                          controller: ctlZipCode,
                          focusNode: focusZipCode,
                          keyboardType: TextInputType.number,
                          maxLength: 5,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.markunread_mailbox),
                              hintText: 'Codigo Postal'),
//                                  : ${_profileInfo.zipCode}'),
//                      readOnly: true,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: RaisedButton(
                            color: HexColor('#0F2C45'),
                            child: Text(
                              'Actualizar',
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: validateForm,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    disposeCtl();
    disposeFocusNode();
    super.dispose();
  }

  disposeFocusNode() {
    focusName.dispose();
    focusFatherName.dispose();
    focusMotherName.dispose();
    focusBirthDate.dispose();
    focusCountry.dispose();
    focusState.dispose();
    focusCity.dispose();
    focusZipCode.dispose();
  }

  disposeCtl() {
    ctlName.dispose();
    ctlFatherName.dispose();
    ctlMotherName.dispose();
    ctlBirthDate.dispose();
    ctlCountry.dispose();
    ctlState.dispose();
    ctlCity.dispose();
    ctlZipCode.dispose();
  }

  void _datePickerDialog(BuildContext context) {
    DatePicker.showDatePicker(
      context,
      minDateTime: DateTime(1940, 01, 1),
      maxDateTime: DateTime.now(),
      initialDateTime: widget.profileInfo != null
          ? DateTime.parse(widget.profileInfo.birthDate)
          : DateTime.now(),
      dateFormat: 'dd-MMMM-yyyy',
      locale: DateTimePickerLocale.es,
      pickerTheme: DateTimePickerTheme(),
      pickerMode: DateTimePickerMode.date, // show TimePicker
      onCancel: () {
        debugPrint('onCancel');
      },
      onConfirm: (dateTime, List<int> index) {
        print(dateTime);
        _profileInfo.birthDate = dateTime.toString();
        ctlBirthDate.text = DateFormat.yMMMMd('es').format(dateTime);
      },
    );
  }

  validateForm() async {
    if (ctlName.text.trim().length == 0) {
      showRequireItem(context, ctlName, focusName, 'Nombres');
    } else if (ctlFatherName.text.trim().length == 0) {
      showRequireItem(
          context, ctlFatherName, focusFatherName, 'Apellido Paterno');
    } else if (ctlBirthDate.text.trim().length == 0) {
      showRequireItem(
          context, ctlBirthDate, focusBirthDate, 'Fecha de Nacimiento');
    } else if (ctlCountry.text.trim().length == 0) {
      showRequireItem(context, ctlCountry, focusCountry, 'País');
    } else if (ctlState.text.trim().length == 0) {
      showRequireItem(context, ctlState, focusState, 'Estado');
    } else if (ctlCity.text.trim().length == 0) {
      showRequireItem(context, ctlCity, focusCity, 'Ciudad');
    } else if (ctlZipCode.text.trim().length < 5) {
      showRequireItem(
          context, ctlZipCode, focusZipCode, 'Código postsl de 5 dígitos');
    } else {
      _profileInfo.name = ctlName.text.trim();
      _profileInfo.fatherSurname = ctlFatherName.text.trim();
      _profileInfo.motherSurname = ctlMotherName.text.trim();
      _profileInfo.country = ctlCountry.text.trim();
      _profileInfo.state = ctlState.text.trim();
      _profileInfo.city = ctlCity.text.trim();
      _profileInfo.zipCode = ctlZipCode.text.trim();
      HttpHandler response = await model.updateProfile(context, _profileInfo);

      if (response.result) {
        GlobalResult result = response.data;
        if (result.result == '1') {
          showSuccessUpdateProfile(context);
        }else{
          showErrorHttpRequest(context);
        }
      }else{
        showErrorHttpRequest(context);
      }
    }
  }
}
